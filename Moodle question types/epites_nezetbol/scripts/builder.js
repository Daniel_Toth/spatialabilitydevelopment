
const PERSPECTIVE = 0;
const ORTHOGRAPHIC = 1;
const ZPOSITIVEVIEW = {vAngle: 90, hAngle: 270};
const ZNEGATIVEVIEW = {vAngle: -90, hAngle: 270};
const XPOSITIVEVIEW = {vAngle: 0, hAngle: 0};
const YPOSITIVEVIEW = {vAngle: 0, hAngle: 90};
const XNEGATIVEVIEW = {vAngle: 0, hAngle: 180};
const YNEGATIVEVIEW = {vAngle: 0, hAngle: 270};
const STANDARDVIEW  = {vAngle: 45, hAngle: 225};

const axis = [XPOSITIVEVIEW, XNEGATIVEVIEW, YPOSITIVEVIEW, YNEGATIVEVIEW,ZPOSITIVEVIEW, ZNEGATIVEVIEW];
const axisName = ["XPOSITIVEVIEW", "XNEGATIVEVIEW", "YPOSITIVEVIEW", "YNEGATIVEVIEW","ZPOSITIVEVIEW", "ZNEGATIVEVIEW"];
const axisColors = ["blue","green","orange","red","white","yellow"];
const colorPalette = ["blue","green","orange","red","white","yellow"];
const Display = {
  TextBook: 0,
  Realistic: 1,
  Simple: 2
};

/*
class RectangleBlock {
  constructor(parameters) {
    this.xStart = parameters.xStart;
    this.xEnd   = parameters.xEnd;
    this.yStart = parameters.yStart;
    this.yEnd   = parameters.yEnd;
    this.zStart = parameters.zStart;
    this.zEnd   = parameters.zEnd;
  }        
}*/
class CubeApplet {
  constructor(domElement,parameters) {
    this.editorMode = 1; /* 0 - No mode
                            1 - Add
                            2 - Delete
                         */
    this.activeColorIndex = 0;
    this.id = parameters.id != undefined ? parameters.id : 0;
    this.displayMode = parameters.displayMode;
    this.construction = parameters.construction;
    this.aimBlock   = this.construction && parameters.aimBlock;
    this.lookAround = parameters.lookAround;
    this.projection = parameters.projection;
    this.defaultDistance = parameters.cameraDistance;
    this.zoomable = parameters.zoomable;
    this.camDrag = { drag: false,
              startX: 0,
              startY: 0};

    this.camPosition = {verticalCamAngle: Math.PI/4,
                            horizontalCamAngle: -Math.PI/4,
                            baseVerticalCamAngle: Math.PI/4,
                            baseHorizontalCamAngle: -Math.PI/4};
    if (parameters.camPos) {
      let vAngle = (90 - parameters.camPos.vAngle)* (Math.PI / 180);
      let hAngle = (parameters.camPos.hAngle + 90)* (Math.PI / 180);
      this.camPosition.verticalCamAngle = vAngle;
      this.camPosition.horizontalCamAngle = hAngle;
      this.camPosition.baseVerticalCamAngle = vAngle;
      this.camPosition.baseHorizontalCamAngle = hAngle;
    }
    this.radius       = this.defaultDistance;
    this.appletWidth  = parameters.width;
    this.appletHeight = parameters.height;
    this.appletRatio  = this.appletHeight / this.appletWidth;

    this.xMin = parameters.limits.xMin;
    this.xMax = parameters.limits.xMax;
    this.yMin = parameters.limits.yMin;
    this.yMax = parameters.limits.yMax;
    this.zMin = parameters.limits.zMin;
    this.zMax = parameters.limits.zMax;

   // this.rectangleBlockArr = Array();
    
    this.scene  = new THREE.Scene();

    

    if (this.projection == ORTHOGRAPHIC) {
      this.camera = new THREE.OrthographicCamera(-16, 16, 16 * this.appletRatio, -16 * this.appletRatio, -100,100 );
    } else {
      this.camera = new THREE.PerspectiveCamera(75 , 1 / this.appletRatio, 1, 10000); 
    }
    
    this.renderer = new THREE.WebGLRenderer({ antialias: true});

    this.pointer = new THREE.Vector2();
    this.pointer.x = undefined;
    this.pointer.y = undefined;

    

    this.scene.background = new THREE.Color( 0xffffff );
    this.material = [];
    if (this.displayMode == Display.Simple){
      colorPalette.forEach((hexcolor, index) => {
        this.material[index] = new THREE.MeshBasicMaterial({color: hexcolor} );
      });
      this.planeMaterial       = new THREE.MeshBasicMaterial({color: 0xffffff, side: THREE.DoubleSide} );
    } else {
      colorPalette.forEach((hexcolor, index) => {
        this.material[index] = new THREE.MeshLambertMaterial({color: hexcolor} );
      });
      this.planeMaterial       = new THREE.MeshPhongMaterial({color: 0xffffff, side: THREE.DoubleSide,shininess: 25} );
    }

    this.transparentMaterial = new THREE.MeshPhongMaterial({color: 0xffffff, opacity: 0.33} );

    

    const color = 0xFFFFFF;


    if (this.displayMode != Display.Simple){
    const light = new THREE.DirectionalLight(color, .5);
    //light.castShadow = true; // default false
    light.position.set(10, 5, 10);
    light.target.position.set(0, 0, 0);

      this.scene.add(light);
    
    
//  scene.add(light.target);.9
  
    const light2 = new THREE.AmbientLight( 0xffffff ); // soft white light
    this.scene.add( light2 );

    }
  // 
    
    this.renderer.shadowMap.enabled = true;
    this.renderer.shadowMap.type = THREE.PCFSoftShadowMap; // default THREE.PCFShadowMap
    this.renderer.setSize( this.appletWidth, this.appletHeight );
    let newDomElement = document.createElement("div");
    let colorPickBar = document.createElement("div");
    let downloadBtn = document.createElement("div");
    this.addBtn     = document.createElement("div");
    this.deleteBtn  = document.createElement("div");

    this.blueBtn   = document.createElement("div");
    this.greenBtn  = document.createElement("div");
    this.orangeBtn = document.createElement("div");
    this.redBtn    = document.createElement("div");
    this.whiteBtn  = document.createElement("div");
    this.yellowBtn = document.createElement("div");

    this.addBtn.innerHTML = "Hozzáadás";
    this.deleteBtn.innerHTML = "Törlés";
    downloadBtn.innerHTML = "Download";

    this.blueBtn.setAttribute("style","background-color: blue; width: 37px; height: 37px; display: inline-block;margin: 0");
    this.greenBtn.setAttribute("style","background-color: green; width: 37px; height: 37px; display: inline-block;margin: 0");
    this.orangeBtn.setAttribute("style","background-color: orange; width: 37px; height: 37px; display: inline-block;margin: 0");
    this.redBtn.setAttribute("style","background-color: red; width: 37px; height: 37px; display: inline-block;margin: 0");
    this.whiteBtn.setAttribute("style","background-color: white; width: 37px; height: 37px; display: inline-block;margin: 0");
    this.yellowBtn.setAttribute("style","background-color: yellow; width: 37px; height: 37px; display: inline-block;margin: 0");

    this.blueBtn.classList.add('builder-funcbtn','btn', 'btn-secondary');
    this.greenBtn.classList.add('builder-funcbtn','btn', 'btn-secondary');
    this.orangeBtn.classList.add('builder-funcbtn','btn', 'btn-secondary');
    this.redBtn.classList.add('builder-funcbtn','btn', 'btn-secondary');
    this.whiteBtn.classList.add('builder-funcbtn','btn', 'btn-secondary');
    this.yellowBtn.classList.add('builder-funcbtn','btn', 'btn-secondary');
    
    downloadBtn.classList.add('builder-funcbtn','btn', 'btn-secondary');
    this.addBtn.classList.add('builder-funcbtn','btn', 'btn-secondary');
    this.deleteBtn.classList.add('builder-funcbtn','btn', 'btn-secondary');

    this.renderer.domElement.style.outline = "rgb(143 149 158) solid 1px";
    newDomElement.appendChild(this.renderer.domElement);

    this.addBtn.addEventListener('mousedown', this.setAddMode );
    this.deleteBtn.addEventListener('mousedown', this.setDeleteMode );
    downloadBtn.addEventListener('mousedown', this.downloadBody );

    this.blueBtn.colorIndex = 0;
    this.blueBtn.addEventListener('mousedown', this.setActiveColor);

    this.greenBtn.colorIndex = 1;
    this.greenBtn.addEventListener('mousedown', this.setActiveColor);

    this.orangeBtn.colorIndex = 2;
    this.orangeBtn.addEventListener('mousedown', this.setActiveColor);

    this.redBtn.colorIndex = 3;
    this.redBtn.addEventListener('mousedown', this.setActiveColor);

    this.whiteBtn.colorIndex = 4;
    this.whiteBtn.addEventListener('mousedown', this.setActiveColor);

    this.yellowBtn.colorIndex = 5;
    this.yellowBtn.addEventListener('mousedown', this.setActiveColor);


    newDomElement.style.position = "relative";
    colorPickBar.setAttribute("style","max-width: 600px;display: flex;bottom: -70px;left: 0;right: 0;justify-content: space-between;z-index: 0;align-items: center;");
    
    colorPickBar.appendChild(this.addBtn);
    colorPickBar.appendChild(this.deleteBtn);
    //colorPickBar.appendChild(downloadBtn);
    colorPickBar.appendChild(this.blueBtn);
    colorPickBar.appendChild(this.greenBtn);
    colorPickBar.appendChild(this.orangeBtn);
    colorPickBar.appendChild(this.redBtn);
    colorPickBar.appendChild(this.whiteBtn);
    colorPickBar.appendChild(this.yellowBtn);



    
    

    if (this.construction) {
      newDomElement.appendChild(colorPickBar);
      this.setAddMode();
    }

    
    domElement.appendChild(newDomElement);
    this.raycaster = new THREE.Raycaster();
    
    this.renderer.domElement.addEventListener('mousemove', this.onPointerMove );
    this.renderer.domElement.addEventListener('mousedown', this.onMouseDown );
    this.renderer.domElement.addEventListener('mouseup',   this.onMouseUp );
    this.renderer.domElement.addEventListener('wheel',   this.onMouseWheel );
    this.boxGeometry = new THREE.BoxGeometry(.995, .995, .995 );
    this.planeGeometry = new THREE.PlaneGeometry( 100, 100 );

  /*  const material            = new THREE.MeshPhongMaterial( { color: 0x33aa33,shininess: 0, wireframe: 1} );
    const planeMaterial      = new THREE.MeshPhongMaterial( {color: 0x888888, side: THREE.DoubleSide,shininess: 25} );
    const transparentMaterial = new THREE.MeshPhongMaterial( { color: 0xffffff, opacity: 0.33} );*/
    this.transparentMaterial.transparent = true;
    this.camera.rotation.order = "ZYX";
    this.camera.zoom = parameters.defaultZoom;
   // cube.castShadow = true; //default is false
 //  this.camera.zoom = .5;
/*
    const plane = new THREE.Mesh( this.planeGeometry, this.planeMaterial );
   // plane.receiveShadow = true; //default
    plane.position.z = -0.5;
    plane.blockType = "plane";
    this.scene.add( plane );*/

    

    this.initBody();
    if (parameters.body) {
      this.loadBody(parameters.body);
    }

    const spritemap = new THREE.TextureLoader().load( 'qmarks.png' );
    const spritematerial = new THREE.SpriteMaterial( { map: spritemap } );

    const spritesprite = new THREE.Sprite( spritematerial );
    spritesprite.scale.set(4, 4, 1)
    spritesprite.position.z = 3;
    this.scene.add( spritesprite );

    
    if (parameters.floor) {
      parameters.floor.z = (parameters.floor.z == undefined) ? 0 : parameters.floor.z;
      this.createBuildingFloor(parameters.floor.xStart,parameters.floor.xEnd,parameters.floor.yStart,parameters.floor.yEnd, parameters.floor.z);
    }
    this.camera.updateProjectionMatrix();
    this.animate();
  }

onPointerMove= (event) => {
  if (this.camDrag.drag) {
    this.camPosition.horizontalCamAngle = this.camPosition.baseHorizontalCamAngle - 2*Math.PI*(event.layerX - this.camDrag.startX)/this.appletWidth;
    this.camPosition.verticalCamAngle   = this.camPosition.baseVerticalCamAngle   - 2*Math.PI*(event.layerY - this.camDrag.startY)/this.appletWidth;
  }
  this.pointer.x =   (event.layerX / this.appletWidth ) * 2 - 1;
  this.pointer.y = - (event.layerY / this.appletHeight) * 2 + 1;
}

onMouseDown = (event) => {
  this.camDrag.startX = event.layerX;
  this.camDrag.startY = event.layerY;

  if (this.lookAround) {
    this.camDrag.drag = true;
  }


  this.startTime = performance.now()
  
  
}

onMouseUp = (event) =>  {
  this.camPosition.baseHorizontalCamAngle = this.camPosition.horizontalCamAngle;
  this.camPosition.baseVerticalCamAngle = this.camPosition.verticalCamAngle;
  this.camDrag.drag = false;
  this.endTime = performance.now()
  if (this.construction && (this.endTime - this.startTime) < 300) {
   switch (this.editorMode) {
     case 1:
      this.addBlockByMouse("buildingBlock","builder", this.material[this.activeColorIndex]);
      break;
    case 2:
      this.removeBlockByMouse();
      break;
     default:
       break;
   }
    
    
  }
}

onMouseWheel = (event) => {
  event.preventDefault();
  if (this.zoomable) {
    if (this.projection == ORTHOGRAPHIC) {
      this.camera.zoom *= (1-event.deltaY * 0.0025);
    } else {
      this.radius  *= (1+event.deltaY * 0.0025);
    }
    
    
    this.camera.updateProjectionMatrix();
  }
}

getBody() {
  return JSON.stringify(this.body);
}

downloadBody = (event) => {
  var element = document.createElement('a');
  element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(JSON.stringify(this.body)));
  element.setAttribute('download', Date.now()+".json");
  element.style.display = 'none';
  document.body.appendChild(element);
  element.click();
  document.body.removeChild(element);
}

setActiveColor = (event) => {
  let index = event.currentTarget.colorIndex;
  if (index >= 0 && index < colorPalette.length) {
    this.activeColorIndex = index;
  }
}

setAddMode = (event) => {
  this.editorMode = 1;
  this.deleteBtn.classList.remove("active", "btn-primary");
  this.deleteBtn.classList.add("btn-secondary");

  this.addBtn.classList.remove("btn-secondary");
  this.addBtn.classList.add("active", "btn-primary");
}

setDeleteMode = (event) => {
  this.editorMode = 2;

  this.addBtn.classList.remove("active", "btn-primary");
  this.addBtn.classList.add("btn-secondary");

  this.deleteBtn.classList.remove("btn-secondary");
  this.deleteBtn.classList.add("active", "btn-primary");
}

initBody() {
  this.body = Array();
  for (var x=0; x<=this.xMax-this.xMin; x++) {
    this.body[x] = Array();
    for (var y=0; y<=this.yMax-this.yMin; y++) {
      this.body[x][y] = Array();
      for (var z=0; z<=this.zMax-this.zMin; z++) {
        this.body[x][y][z] = 0;
      }
    }
  }
}
/*
addRectangleBlock(rectBlock) {
  this.rectangleBlockArr.push(rectBlock);
  this.createRectangleBlock(rectBlock.xStart, rectBlock.xEnd, rectBlock.yStart, rectBlock.yEnd, rectBlock.zStart, rectBlock.zEnd);
}
*/
loadBody(bodyJSON) {
  this.body = JSON.parse(bodyJSON);
  for (var x=0; x<=this.xMax-this.xMin; x++) {
    for (var y=0; y<=this.yMax-this.yMin; y++) {
      for (var z=0; z<=this.zMax-this.zMin; z++) {
        if (this.body[x][y][z] > 0) {
          
            this.addCube({
            x: x+this.xMin,
            y: y+this.yMin,
            z: z+this.zMin,
            name: "building",
            blockType: "builder",
            material: this.material[this.body[x][y][z] - 1],
            offsetZ: 0
          });

          
        }
      }
    }
  }  
}


addCube(parameters) {
  
  if (parameters.blockType == "builder") {
    this.body[parameters.x - this.xMin][parameters.y - this.yMin][parameters.z - this.zMin] = this.activeColorIndex + 1;
  }


  let matLineBasic = new THREE.LineBasicMaterial( { color: 0x4080ff } );
  let newCube = new THREE.Mesh( this.boxGeometry, parameters.material );


  newCube.position.x = parameters.x;
  newCube.position.y = parameters.y;
  newCube.position.z = parameters.z + parameters.offsetZ;
  newCube.name = parameters.name + Math.random();
  newCube.castShadow = true;
  newCube.receiveShadow = true;
  newCube.blockType = parameters.blockType;

  this.scene.add( newCube );
  

  let gem = new THREE.BoxGeometry(newCube.geometry.parameters.width + 0, 
                                  newCube.geometry.parameters.height + 0*this.radius/500,
                                  newCube.geometry.parameters.depth + 0*this.radius/500);

  const edges = new THREE.EdgesGeometry( gem );
  gem.width +=1;
  let line;
  if (this.display == Display.TextBook) {
    line = new THREE.LineSegments( edges, new THREE.LineBasicMaterial( { color: 0x000000,linewidth: 1 } ) );
  } else {
    line = new THREE.LineSegments( edges, new THREE.LineBasicMaterial( { color: 0x000000,linewidth: 1 } ) );
  }
  
  line.position.x = parameters.x;
  line.position.y = parameters.y;
  line.position.z = parameters.z + parameters.offsetZ;
  line.name = "edge"+newCube.name;
  this.scene.add( line );
  
 // this.blockArray.push(newCube);
 // this.blockEdgeArray.push(line)
}

addObserver(view, color = "green") {


  try {
    let epites_nezetbolImagePath = typeof(epites_nezetbolImagePath) == 'undefined' ? "" : epites_nezetbolImagePath;
  }
  catch(err) {
    let epites_nezetbolImagePath = ""
  }

  
  let qmap = new THREE.ImageUtils.loadTexture(epites_nezetbolImagePath + color + 'turtle.svg' );

  
  let qmaterial = new THREE.MeshLambertMaterial( { map: qmap,side: THREE.DoubleSide,transparent: true } );


  const wgeometry = new THREE.PlaneGeometry( 1, 1 );

  let qsprite =  new THREE.Mesh( wgeometry, qmaterial );

  qsprite.scale.x = 2;
  qsprite.scale.y = 2;
  qsprite.scale.z = 2;
  qsprite.position.x = (this.xMax+1) * Math.cos(view.hAngle * Math.PI / 180) * Math.cos(view.vAngle * Math.PI / 180);
  qsprite.position.y = (this.yMax+1) * Math.sin(view.hAngle * Math.PI / 180) * Math.cos(view.vAngle * Math.PI / 180);
  qsprite.position.z = (this.zMax) * Math.sin(view.vAngle * Math.PI / 180);

  qsprite.rotation.x = (Math.PI / 2) * Math.sin(view.hAngle* Math.PI / 180)+ Math.sin(view.vAngle* Math.PI / 180)*(Math.PI / 2)  * Math.sin(view.hAngle* Math.PI / 180) ;
  qsprite.rotation.y = -(Math.PI / 2) * Math.cos(view.hAngle* Math.PI / 180)- Math.sin(view.vAngle* Math.PI / 180)*(Math.PI / 2)   * Math.cos(view.hAngle* Math.PI / 180);
  qsprite.rotation.z = view.hAngle* Math.PI / 180;


  this.scene.add( qsprite );
}

removeCube(cube) {
  
  let name = "edge"+cube.name;

  if (cube.blockType == "builder") {
    this.body[cube.position.x - this.xMin][cube.position.y - this.yMin][cube.position.z - this.zMin] = 0;
  }
  this.scene.remove(cube);
  var selectedObject = this.scene.getObjectByName(name);
    if (selectedObject != undefined) {
      this.scene.remove( selectedObject );
    }



   
  
 // this.scene.remove(this.blockEdgeArray[index]);

  

 // this.blockArray.splice(index,10);
 // this.blockEdgeArray.splice(index,10);
}

isInBuildingSpace(cubePosition) {
  return ((cubePosition.x >= this.xMin && cubePosition.x <= this.xMax) &&
  (cubePosition.y >= this.yMin && cubePosition.y <= this.yMax) &&
  (cubePosition.z >= this.zMin && cubePosition.z <= this.zMax));

}

addBlockByMouse(name, blockType, material) {
  if (this.pointer.x == undefined || this.pointer.y == undefined) { return;}
  this.raycaster.setFromCamera( this.pointer, this.camera );
  const intersects = this.raycaster.intersectObjects( this.scene.children );
  for( var i = 0; i < intersects.length; i++){ 
      if ( intersects[i].object.blockType != "builder" && intersects[i].object.blockType != "builderFloor" ) { 
        intersects.splice(i, 1); 
        i--;
      }
  }
  if ( intersects.length > 0 ) {
      var index = Math.floor( intersects[0].faceIndex / 2 );
      let newCubePosition = { x:Math.floor(intersects[0].object.position.x),
                              y:Math.floor(intersects[0].object.position.y),
                              z:Math.floor(intersects[0].object.position.z)};
      switch (index) {
          case 0:
            newCubePosition.x++;
            break;
          case 1:
            newCubePosition.x--;
            break;
          case 2:
            newCubePosition.y++;
            break;
          case 3:
            newCubePosition.y--;
            break;
          case 4:
            newCubePosition.z++;
            break;
          case 5: 
            newCubePosition.z--;
      }

      if (this.isInBuildingSpace(newCubePosition)) {
        this.addCube({
          x: newCubePosition.x,
          y: newCubePosition.y,
          z: newCubePosition.z,
          name: name,
          blockType: blockType,
          material: material,
          offsetZ: 0,
        });
      }
  } 
}

removeBlockByMouse() {
  if (this.pointer.x == undefined || this.pointer.y == undefined) { return;}
  this.raycaster.setFromCamera( this.pointer, this.camera );
  const intersects = this.raycaster.intersectObjects( this.scene.children );
  for( var i = 0; i < intersects.length; i++){ 
      if ( intersects[i].object.blockType != "builder") { 
        intersects.splice(i, 1); 
        i--;
      }
  }
  if ( intersects.length > 0 ) {
    this.removeCube(intersects[0].object);
  } 
}

/*
createRectangleBlock(xStart, xEnd, yStart, yEnd, zStart, zEnd) {
  this.boxGeometry = new THREE.BoxGeometry(1, 1, 1 );
  for (let x = xStart; x<=xEnd; x++) {
    for (let y = yStart; y<=yEnd; y++) {
      for (let z = zStart; z<=zEnd; z++) {
        
        if (x == xStart || x == xEnd ||
            y == yStart || y == yEnd ||
            z == zStart || z == zEnd) {

              
              this.addCube({
                x: x,
                y: y,
                z: z,
                name: "buildingBlock",
                blockType: "builder",
                material: this.material,
                offsetZ: 0, 
              });
              
            }
      }
    }
  }
  this.boxGeometry = new THREE.BoxGeometry(1, 1, 1 );
}
*/
createBuildingFloor(xStart, xEnd, yStart, yEnd, z) {
  this.boxGeometry = new THREE.BoxGeometry(.995, .995, .1 );
  for (let x = xStart; x<=xEnd; x++) {
    for (let y = yStart; y<=yEnd; y++) {                    
              this.addCube({
                x: x,
                y: y,
                z: z-1,
                name: "buildingBlock",
                blockType: "builderFloor",
                material: this.planeMaterial,
                offsetZ: .45, 
              });
    }
  }
  this.boxGeometry = new THREE.BoxGeometry(.995, .995, .995 );
}


animate = () => {
//   this.camera = new THREE.OrthographicCamera(-this.radius, this.radius, this.radius * this.appletRatio, -this.radius * this.appletRatio, -100,100 );
  this.camera.position.x =   this.radius * Math.sin(this.camPosition.verticalCamAngle) * Math.sin(this.camPosition.horizontalCamAngle);
  this.camera.position.y = - this.radius * Math.sin(this.camPosition.verticalCamAngle) * Math.cos(this.camPosition.horizontalCamAngle);
  this.camera.position.z =   this.radius * Math.cos(this.camPosition.verticalCamAngle) + 3 * Math.sin(this.camPosition.verticalCamAngle);
  this.camera.rotation.x = this.camPosition.verticalCamAngle;
  this.camera.rotation.y = 0;
  this.camera.rotation.z = this.camPosition.horizontalCamAngle;
  if (this.projection == ORTHOGRAPHIC) {
   // this.camera.zoom = this.defaultDistance/(this.radius);
  }
  this.camera.updateMatrixWorld();

  if (this.aimBlock && this.editorMode == 1) {
    var selectedObject = this.scene.getObjectByProperty("blockType","transparent");
    if (selectedObject != undefined) {
      this.removeCube( selectedObject );

    }
    this.addBlockByMouse("transparent","transparent", this.transparentMaterial);
  }



    this.renderer.render( this.scene, this.camera );
  if (this.lookAround || this.zoomable || this.construction) {
    requestAnimationFrame( this.animate );
  }
  
}    
}

