

window.addEventListener("load", (event) => {


    if (document.location.pathname.includes("review") && !document.location.pathname.includes("preview")) {
        if (!window.location.href.includes("showall=")) {
            window.location.href += "&showall=0";
        }
    }

    //contains all 
    let appletArr = [];
    appletList.forEach(loadApplet);
    function loadApplet(applet, index) {
        let bodyJSON = applet.bodyJSON;
        let element = document.getElementById(applet.containerElementName);
        let parentElement = element.parentElement;

        element.setAttribute("style","position: relative;border: 1px;display: inline-block; float: left; margin: 5px;transform: translate(-15px, -5px);");
        parentElement.setAttribute("appid", index);


        if (document.location.pathname.includes("review") && !document.location.pathname.includes("preview")) {
            applet.options.lookAround = true;
            parentElement.parentElement.querySelector(".outcome").style.display="none";
        }
    
        let newApplet = new CubeApplet(element, applet.options);

       
        let axisIndex = axis.indexOf(applet.options.camPos);
        if (axisIndex != -1) {
            var elem = document.createElement("img");
            elem.setAttribute("style","position: absolute;top:0;left:0;transform: rotate(270deg);");
            elem.setAttribute("src", epites_nezetbolImagePath + axisColors[axisIndex] + 'turtle.svg');
            elem.setAttribute("height", "50");
            elem.setAttribute("width", "50");
            element.appendChild(elem); 
            //alert(axisIndex + " " +axisColors[axisIndex]);
        } else {
            for (let i = 0;i<6; i++) {
                if (VIEWS[i]) {
                    newApplet.addObserver(axis[i], axisColors[i]);
                }
            }
                
        }

        

        /*
        {
                id: applet.containerElementName,
                width: 600,
                height: 600,
                body: bodyJSON,
                cameraDistance: 25,
                defaultZoom: 2,
                projection:ORTHOGRAPHIC,
                displayMode: Display.TextBook,
                lookAround: true,
                aimBlock: true,
                zoomable: true,
                construction: true,
                floor: {xStart: -5,xEnd: 5,yStart: -5, yEnd: 5},
                limits: {xMin: -5,xMax: 5,yMin: -5,yMax: 5,zMin: 0,zMax: 10},
                camPos: STANDARDVIEW,
        }
        
        */
        /*newApplet.addObserver(XPOSITIVEVIEW, "red");
        newApplet.addObserver(YPOSITIVEVIEW, "blue");
        newApplet.addObserver(XNEGATIVEVIEW, "green");
        newApplet.addObserver(ZPOSITIVEVIEW);*/

        appletArr.push(newApplet);


        if (applet.options.construction == true) {
            
            parentElement.addEventListener("mouseup", updateBodyInput);
            parentElement.addEventListener("mouseleave", updateBodyInput);
            parentElement.addEventListener("mouseenter", updateBodyInput);
           
        }
    }

    function shiftToXYCorner(bodyArray) {
        let xO,yO,zO, copyArray;
        xO = yO = zO = 0;
        copyArray = bodyArray.slice();
        xOsearch: {
            for (xO = 0; xO < 11; xO++) {
                for (let y = 0; y < 11; y++) {
                    for (let z = 0; z < 11; z++) {
                        if (bodyArray[xO][y][z] != 0) {
                            break xOsearch;
                        }
                    }
                }
            }
        }

        yOsearch: {
            for (yO = 0; yO < 11; yO++) {
                for (let x = 0; x < 11; x++) {
                    for (let z = 0; z < 11; z++) {
                        if (bodyArray[x][yO][z] != 0) {
                            break yOsearch;
                        }
                    }
                }
            }
        }

        zOsearch: {
            for (zO = 0; zO < 11; zO++) {
                for (let y = 0; y < 11; y++) {
                    for (let x = 0; x < 11; x++) {
                        if (bodyArray[x][y][zO] != 0) {
                            break zOsearch;
                        }
                    }
                }
            }
        }

        if (xO == 11 || yO == 11 || zO == 11) {
            xO = yO = zO = 0;
        }


        for (let x = 0; x < 11; x++) {
            for (let y = 0; y < 11; y++) {
                for (let z = 0; z < 11; z++) {
                    if ((x+xO) > 10 || (y+yO) > 10 || (z+zO) > 10) {
                        copyArray[x][y][z] = 0;
                    } else {
                        copyArray[x][y][z] = bodyArray[x+xO][y+yO][z+zO];
                    } 
                }
            }
        }

        return copyArray;
    }

    function getView(bodyArray, viewAxis) {
        let mapchararr = ".bgorwy";
        let res = "";

        switch(viewAxis) {
            case axis[1]: //X negative
                for (let z = 10; z >= 0; z--) {
                    for (let y = 10; y >= 0; y--) {
                        let x = 0;
                        while (bodyArray[x][y][z] == 0 && x<10){
                            x++;
                        }
                        res += mapchararr[bodyArray[x][y][z]];
                    }
                    res += "\n";
                }
                break;
            case axis[0]: //X positive
                for (let z = 10; z >= 0; z--) {
                    for (let y = 0; y < 11; y++) {
                        let x = 10;
                        while (bodyArray[x][y][z] == 0 && x>0){
                            x--;
                        }
                        res += mapchararr[bodyArray[x][y][z]];
                    }
                    res += "\n";
                }
                break;
            case axis[3]: //Y negative
                for (let z = 10; z >= 0; z--) {
                    for (let x = 0; x < 11; x++) {
                        let y = 0;
                        while (bodyArray[x][y][z] == 0 && y<10){
                            y++;
                        }
                        res += mapchararr[bodyArray[x][y][z]];
                    }
                    res += "\n";
                }
                break;
            case axis[2]: //Y positive
                for (let z = 10; z >= 0; z--) {
                    for (let x = 10; x >=0; x--) {
                        let y = 10;
                        while (bodyArray[x][y][z] == 0 && y>0){
                            y--;
                        }
                        res += mapchararr[bodyArray[x][y][z]];
                    }
                    res += "\n";
                }
                break;
            case axis[5]: //Z negative
                for (let y = 0; y < 11; y++) {
                    for (let x = 0; x < 11; x++) {
                        let z = 0;
                        while (bodyArray[x][y][z] == 0 && z<10){
                            z++;
                        }
                        res += mapchararr[bodyArray[x][y][z]];
                    }
                    res += "\n";
                }
                break;
            case axis[4]: //Z positive
                for (let y = 10; y >= 0; y--) {
                    for (let x = 0; x < 11; x++) {
                        let z = 10;
                        while (bodyArray[x][y][z] == 0 && z>0){
                            z--;
                        }
                        res += mapchararr[bodyArray[x][y][z]];
                    }
                    res += "\n";
                }
                break;
            default:
                alert("hiba a getView  fuggvenyben" + viewAxis);
          }
        return res;
    }
    

    function updateBodyInput(e) {

        let elementID;
        let appElement = e.srcElement.parentElement.parentElement.parentElement.parentElement.querySelector(".appid");

        if (appElement == null) {
            elementID = e.srcElement.parentElement.parentElement.getAttribute("id");
        } else {
            elementID = appElement.getAttribute("id");
        }
        if (elementID == undefined || elementID == "") {
            return false;
        }
        let truefalseswitch = document.getElementById(elementID).parentElement.querySelector("input." + elementID);
        let element;
        for (let i = 0; i < appletArr.length; i++) {
            if (appletArr[i].id == elementID) {
                element = appletArr[i]; 
                break;
            }
        }

        let userSolution = JSON.parse(element.getBody());
        let solutionArr = JSON.parse(eval("SOLUTION" + elementID));

        userSolution = shiftToXYCorner(userSolution);

        solutionArr = shiftToXYCorner(solutionArr);


        let successfull = true;
        for (let index = 0; index < 6; index++) {
            if (VIEWS[index]) {
        
                document.getElementById(axisName[index] + elementID).querySelectorAll(".feedbackicon").forEach(e => e.remove());
                let feedbackicon = document.createElement("div");
                feedbackicon.setAttribute("class","feedbackicon");
                if (getView(solutionArr, axis[index]) != getView(userSolution , axis[index])) {
                    successfull = false;
              
                    feedbackicon.setAttribute("style","box-shadow: none;width: 27px;height: 27px;position: absolute;top: 15px;right: 15px;z-index: 1;background-image: url(\"data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' xml:space='preserve'  viewBox='0 0 461 461'%3E%3Cpath fill='%23880000' d='M285 230 456 59c6-6 6-16 0-22L424 5a16 16 0 0 0-22 0L230 176 59 5a16 16 0 0 0-22 0L5 37c-7 6-7 16 0 22l171 171L5 402c-6 6-6 15 0 21l32 33a16 16 0 0 0 22 0l171-171 172 171a16 16 0 0 0 21 0l33-33c6-6 6-15 0-21L285 230z'/%3E%3C/svg%3E\");")
                    
                } else {
                    feedbackicon.setAttribute("style","box-shadow:none;color: #053400;position: absolute;top: 15px;right: 15px;margin-left: 20px;margin-top: 20px;width: 30px;height: 15px;border-bottom: solid 7px currentColor;border-left: solid 7px currentColor;-webkit-transform: rotate(-45deg);transform: rotate(-45deg);z-index: 10;box-shadow: 0;")
                }
                document.getElementById(axisName[index] + elementID).append(feedbackicon);

                
            }
        }

        let questionState;
        if (successfull) {
            questionState = {correct: 1, result: element.getBody()};
        } else {
            questionState = {correct: 0, result: element.getBody()};
        }
        truefalseswitch.value = JSON.stringify(questionState);




        
       // document.getElementById("id_feedbacktrue").value = newApplet.getBody();
    }

});