<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * True-false question renderer class.
 *
 * @package    qtype
 * @subpackage epites_nezetbol
 * @copyright  2009 The Open University
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */


defined('MOODLE_INTERNAL') || die();


/**
 * Generates the output for true-false questions.
 *
 * @copyright  2009 The Open University
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class qtype_epites_nezetbol_renderer extends qtype_renderer {

    
    public function formulation_and_controls(question_attempt $qa,
            question_display_options $options) {
        global $PAGE, $CFG;
        
     
        echo "<script src=" . $CFG->wwwroot. '/question/type/epites_nezetbol/scripts/three.min.js' . "></script>";
        echo "<script src=" . $CFG->wwwroot. '/question/type/epites_nezetbol/scripts/builder.js' . "></script>";
      //  $PAGE->requires->js(new moodle_url($CFG->wwwroot. '/question/type/epites_nezetbol/scripts/three.min.js'));
       // $PAGE->requires->js(new moodle_url($CFG->wwwroot. '/question/type/epites_nezetbol/scripts/builder.js'));
        $PAGE->requires->js(new moodle_url($CFG->wwwroot. '/question/type/epites_nezetbol/d.js'));
        $question = $qa->get_question();
        $response = $qa->get_last_qt_var('answer', '');
        $responseEncoded = json_decode($response);


        $inputname = $qa->get_qt_field_name('answer');

        $trueattributes = array(
            'type' => 'hidden',
            'name' => $inputname,
            'value' => $response,
            'id' => $inputname . 'true',
        );

      /*  $falseattributes = array(
            'type' => 'radio',
            'name' => $inputname,
            'value' => 0,
            'id' => $inputname . 'false',
        );*/

        if ($options->readonly) {
            $trueattributes['disabled'] = 'disabled';
            $falseattributes['disabled'] = 'disabled';
        }

        // Work out which radio button to select (if any).
 /*       $truechecked = false;
        $falsechecked = false;
        $responsearray = array();
        if ($response) {
            $trueattributes['checked'] = 'checked';
            $truechecked = true;
            $responsearray = array('answer' => 1);
        } else if ($response !== '') {
            $falseattributes['checked'] = 'checked';
            $falsechecked = true;
            $responsearray = array('answer' => 1);
        }
*/
        // Work out visual feedback for answer correctness.
        $trueclass = '';
        $falseclass = '';
        $truefeedbackimg = '';
        $falsefeedbackimg = '';
        if ($options->correctness) {
            if ($truechecked) {
                $trueclass = ' ' . $this->feedback_class((int) $question->rightanswer);
                $truefeedbackimg = $this->feedback_image((int) $question->rightanswer);
            } else if ($falsechecked) {
                $falseclass = ' ' . $this->feedback_class((int) (!$question->rightanswer));
                $falsefeedbackimg = $this->feedback_image((int) (!$question->rightanswer));
            }
        }

        /*$radiotrue = html_writer::empty_tag('input', $trueattributes) .
                html_writer::tag('label', get_string('true', 'qtype_epites_nezetbol'),
                array('for' => $trueattributes['id'], 'class' => 'ml-1'));*/
        $elementid = "randomid" . bin2hex(random_bytes(10));
        $trueattributes["class"] .= $elementid;
        $radiotrue = html_writer::empty_tag('input', $trueattributes);

        $radiofalse = html_writer::empty_tag('input', $falseattributes) .
                html_writer::tag('label', get_string('false', 'qtype_epites_nezetbol'),
                array('for' => $falseattributes['id'], 'class' => 'ml-1'));

        $questionData = json_decode($question->truefeedback);
        $axisColors = ["XPOSITIVEVIEW", "XNEGATIVEVIEW", "YPOSITIVEVIEW", "YNEGATIVEVIEW","ZPOSITIVEVIEW", "ZNEGATIVEVIEW"];

        $turtleorturtles = sizeof($questionData->choosedViews) == 1 ? "A nézet a teknős felől készült." : "Az nézetek a színes teknősök felől készültek.";
        $result = "
        
        <h2>Az adott nézetek szerint építsd meg a kockaépítményt. </h2>
        <p>$turtleorturtles Az építményt a sík bármelyik részén megépítheted.</p>
        <div id=\"" . $elementid . "\" class=\"appid\"></div>";
        for ($i = 0; $i < sizeof($questionData->choosedViews); $i++) {
            if ($questionData->choosedViews[$i]) {
                $result .= "<div id=\"".$axisColors[$i]. $elementid . "\" class=\"appid\"></div>";
            }
        }
        $jsonViews = json_encode($questionData->choosedViews);
        $result .= "<script>
        const VIEWS = JSON.parse(\"$jsonViews\");
        const SOLUTION$elementid = \"" . $questionData->bodyJSON . "\";
        epites_nezetbolImagePath  = typeof(epites_nezetbolImagePath) == 'undefined' ? \"" . $CFG->wwwroot. "/question/type/epites_nezetbol/pix/\" : epites_nezetbolImagePath;
        appletList = typeof(appletList) == 'undefined' ? [] : appletList;
        appletList.push({
            containerElementName: \"" . $elementid . "\",
            bodyJSON: SOLUTION$elementid,
            options: {
                id: \"" . $elementid . "\",
                width: 600,
                height: 600,
                body: \"".$responseEncoded->result."\",
                cameraDistance: 25,
                defaultZoom: 2,
                projection:ORTHOGRAPHIC,
                displayMode: Display.TextBook,
                lookAround: true,
                aimBlock: true,
                zoomable: true,
                construction: true,
                floor: {xStart: -5,xEnd: 5,yStart: -5, yEnd: 5},
                limits: {xMin: -5,xMax: 5,yMin: -5,yMax: 5,zMin: 0,zMax: 10},
                camPos: STANDARDVIEW,
        }
        });";

        for ($i = 0; $i < sizeof($questionData->choosedViews); $i++) {
            if ($questionData->choosedViews[$i]) {
            $floor = "{xStart: -5,xEnd: 5,yStart: -5, yEnd: 5,z:0}";
            if ($axisColors[$i] == "ZNEGATIVEVIEW") {
                $floor = "{xStart: -5,xEnd: 5,yStart: -5, yEnd: 5,z:10}";
            }
            $result .= "
            appletList.push({
                containerElementName: \"" . $axisColors[$i]. $elementid . "\",
                bodyJSON: \"" . $questionData->bodyJSON . "\",
                options: {
                    id: \"" . $axisColors[$i]. $elementid . "\",
                    width: 290,
                    height: 290,
                    body: \"" . $questionData->bodyJSON. "\",
                    cameraDistance: 20,
                    defaultZoom: 3,
                    projection:ORTHOGRAPHIC,
                    displayMode: Display.Simple,
                    lookAround: false,
                    aimBlock: false,
                    zoomable: false,
                    construction: false,
                    floor: $floor,
                    limits: {xMin: -5,xMax: 5,yMin: -5,yMax: 5,zMin: 0,zMax: 10},
                    camPos: $axisColors[$i] ,
            }
            });";
            }
        }

        $result .= "</script>";
        
       /* $result .= html_writer::tag('div',$question->truefeedback,
                array('class' => 'qtext'));*/

        $result .= html_writer::start_tag('div', array('class' => 'ablock'));
       /* $result .= html_writer::tag('div', get_string('selectone', 'qtype_epites_nezetbol'),
                array('class' => 'prompt'));*/

        $result .= html_writer::start_tag('div', array('class' => 'answer'));
        $result .= html_writer::tag('div', $radiotrue . ' ' . $truefeedbackimg,array('class' => 'r0' . $trueclass));
       /* $result .= html_writer::tag('div', $radiofalse . ' ' . $falsefeedbackimg,
                array('class' => 'r1' . $falseclass));*/
        $result .= html_writer::end_tag('div'); // Answer.

        $result .= html_writer::end_tag('div'); // Ablock.

        if ($qa->get_state() == question_state::$invalid) {
            $result .= html_writer::nonempty_tag('div',
                    $question->get_validation_error($responsearray),
                    array('class' => 'validationerror'));
        }

        return $result;
    }

    public function specific_feedback(question_attempt $qa) {
        $question = $qa->get_question();
        $response = $qa->get_last_qt_var('answer', '');

        if ($response) {
            return $question->format_text($question->truefeedback, $question->truefeedbackformat,
                    $qa, 'question', 'answerfeedback', $question->trueanswerid);
        } else if ($response !== '') {
            return $question->format_text($question->falsefeedback, $question->falsefeedbackformat,
                    $qa, 'question', 'answerfeedback', $question->falseanswerid);
        }
    }

    public function correct_response(question_attempt $qa) {
        $question = $qa->get_question();

        if ($question->rightanswer) {
            return get_string('correctanswertrue', 'qtype_epites_nezetbol');
        } else {
            return get_string('correctanswerfalse', 'qtype_epites_nezetbol');
        }
    }
}
