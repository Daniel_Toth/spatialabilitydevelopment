<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'qtype_epites_nezetbol', language 'en', branch 'MOODLE_20_STABLE'
 *
 * @package    qtype
 * @subpackage epites_nezetbol
 * @copyright  1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['correctanswer'] = 'Correct answer';

$string['correctanswerfalse'] = 'The correct answer is \'False\'.';
$string['correctanswertrue'] = 'The correct answer is \'True\'.';
$string['false'] = 'False';
$string['feedbackfalse'] = 'Feedback for the response \'False\'.';
$string['feedbacktrue'] = 'Feedback for the response \'True\'.';
$string['body'] = 'Test';

$string['pleaseselectananswer'] = 'Please select an answer.';
$string['selectone'] = 'Select one:';
$string['true'] = 'True';
$string['pluginname'] = 'epites_nezetbol';
$string['pluginname_help'] = 'In response to a question (that may include an image) the respondent chooses from true or false.';
$string['pluginname_link'] = 'question/type/epites_nezetbol';
$string['pluginnameadding'] = 'Adding a epites_nezetbol question';
$string['pluginnameediting'] = 'Editing a epites_nezetbol question';
$string['pluginnamesummary'] = 'A simple form of multiple choice question with just the two choices \'True\' and \'False\'.';
$string['privacy:metadata'] = 'The epites_nezetbol question type plugin does not store any personal data.';
