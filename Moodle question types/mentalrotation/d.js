window.addEventListener("load", (event) => {

    if (document.location.pathname.includes("review") && !document.location.pathname.includes("preview")) {
        if (!window.location.href.includes("showall=")) {
            window.location.href += "&showall=0";
        }
    }

    function rand(a) {
        var t = a += 0x6D2B79F5;
        t = Math.imul(t ^ t >>> 15, t | 1);
        t ^= t + Math.imul(t ^ t >>> 7, t | 61);
        return ((t ^ t >>> 14) >>> 0) / 4294967296;
    }

    function getRandomExcept(l, h, e, seed) {
        if (l == h && h == e) {
            return e;
        }
        let t;
        do {
            t = Math.floor(rand(seed++) * (h - l + 1) + l);
        } while (t == e);
        return t;
    }

    function generateSnake(seed, limits, version) {
        let snake = Array();

        let x, y, z;
        for (x = 0; x <= limits.xMax - limits.xMin; x++) {
            snake[x] = Array();
            for (y = 0; y <= limits.yMax - limits.yMin; y++) {
                snake[x][y] = Array();
                for (z = 0; z <= limits.zMax - limits.zMin; z++) {
                    snake[x][y][z] = 0;
                }
            }
        }

        x = Math.floor((limits.xMax - limits.xMin) / 2);
        y = x;
        z = 6;

        let xt, yt, zt, xr, yr, zr;

        xt = x;
        yt = y;
        zt = z;

        let inverse = [1, 0, 3, 2, 5, 4];
        let axisone = getRandomExcept(0, 5, 100, seed++);

        let axistwo;
        do {
            axistwo = getRandomExcept(0, 5, inverse[axisone], seed++);
        } while (axisone == axistwo);

        let xOff = [1, -1, 0, 0, 0, 0];
        let yOff = [0, 0, 1, -1, 0, 0];
        let zOff = [0, 0, 0, 0, 1, -1];
        let oneLength = getRandomExcept(2, 5, 100, seed++);
        let twoLength = getRandomExcept(2, 5, 100, seed++);


        seed++;
        /*
                if (version == 20) {
        
                    let temp = axisone;
                    axisone = axistwo;
                    axistwo = temp;
                    if (getRandomExcept(1,1000,1,seed) % 2) {
                        twoLength += getRandomExcept(-1,2,0,seed);
                    } else {
                        oneLength += getRandomExcept(-1,2,0,seed);
                    }
                }
        */
        for (let i = 0; i < oneLength; i++) {
            xt += xOff[axisone];
            yt += yOff[axisone];
            zt += zOff[axisone];
            snake[xt][yt][zt] = 3;
        }

        for (let i = 0; i < twoLength; i++) {
            xt += xOff[axistwo];
            yt += yOff[axistwo];
            zt += zOff[axistwo];
            snake[xt][yt][zt] = 3;
        }

        xt = x;
        yt = y;
        zt = z;


        let pastaxisone = axisone;
        let pastaxistwo = axistwo;
        do {
            axisone = getRandomExcept(0, 5, inverse[pastaxisone], seed++);
        } while (axisone == pastaxisone || axisone == axistwo);


        /* if (version == 1) {
             seed++;
         }*/



        let sinc = 0;
        do {
            axistwo = getRandomExcept(0, 5, inverse[axisone], seed + sinc);
            sinc++;
        } while (axisone == axistwo || axistwo == pastaxisone);


        let oldaxistwo = axistwo;
        let oldaxisone = axisone;
        seed++;
        oneLength = getRandomExcept(2, 5, 100, seed++);

        let oldTwoLength = twoLength;
        twoLength = getRandomExcept(2, 5, oldTwoLength, seed++);


        if (version == 1) {
            do {
                axistwo = getRandomExcept(0, 5, inverse[axisone], seed + sinc);
                sinc++;
            } while (axisone == axistwo || axistwo == pastaxisone || axistwo == oldaxistwo);
        }


        let xyzdimensions = [false,false,false];

        xyzdimensions[Math.floor(axisone/2)] = true;
        xyzdimensions[Math.floor(pastaxisone/2)] = true;
        xyzdimensions[Math.floor(axistwo/2)] = true;
        xyzdimensions[Math.floor(pastaxistwo/2)] = true;
        

        if (xyzdimensions[0] == false) {
            axistwo = getRandomExcept(0, 1, 100, seed+10);
        }
        if (xyzdimensions[1] == false) {
            axistwo = getRandomExcept(2, 3, 100, seed+11);
        }
        if (xyzdimensions[2] == false) {
            axistwo = getRandomExcept(4, 5, 100, seed+12);
        }



        /* if (version == 2) {
             oneLength += getRandomExcept(-1,1,100,seed);
         }*/

        for (let i = 0; i < oneLength; i++) {
            xt += xOff[axisone];
            yt += yOff[axisone];
            zt += zOff[axisone];
            snake[xt][yt][zt] = 3;
        }




        /* if (version == 2) {
             twoLength += getRandomExcept(-1,1,0,seed);
         }*/

        for (let i = 0; i < twoLength; i++) {
            xt += xOff[axistwo];
            yt += yOff[axistwo];
            zt += zOff[axistwo];
            snake[xt][yt][zt] = 3;



        }
        snake[x][y][z] = 3;
        return JSON.stringify(snake);
    }

    let correctIndexOne = getRandomExcept(1, 4, 100, APPSEED);
    let correctIndexTwo = getRandomExcept(1, 4, correctIndexOne, APPSEED + 214);
    let correctShape = generateSnake(APPSEED + 116, { xMin: -7, xMax: 7, yMin: -7, yMax: 7, zMin: -7, zMax: 7 }, 0);
    let shapeOne = generateSnake(APPSEED + 116, { xMin: -7, xMax: 7, yMin: -7, yMax: 7, zMin: -7, zMax: 7 }, 1);
    let shapeTwo = generateSnake(APPSEED + 116, { xMin: -7, xMax: 7, yMin: -7, yMax: 7, zMin: -7, zMax: 7 }, 2);

    let appletArr = [];
    appletList.forEach(loadApplet);

    function loadApplet(applet, index) {
        APPSEED++;

        let bodyJSON = applet.bodyJSON;
        let element = document.getElementById(applet.containerElementName);
        let parentElement = element.parentElement;

        // parentElement.addEventListener('mousemove', checktest);






        element.setAttribute("style", "position: relative;border: 1px;display: inline-block; float: left; margin: 5px;transform: translate(-15px, -5px);overflow: hidden;");
        parentElement.setAttribute("appid", index);

        //  hAngle: getRandomExcept(0,360,1000,APPSEED + 222 + index
        //getRandomExcept(-90,90,100,APPSEED + 111+ index)
        APPSEED += 111;
        let vRandAngle = getRandomExcept(30, 60, 100, APPSEED + index);
        APPSEED += vRandAngle + 21;
        let vRandPhase = getRandomExcept(0, 3, 100, APPSEED + index);
        APPSEED += 222;
        let hRandAngle = getRandomExcept(30, 60, 100, APPSEED + index);
        APPSEED += hRandAngle + 97;
        let hRandPhase = getRandomExcept(0, 3, 100, APPSEED + index);
        APPSEED += 31;
        applet.options.camPos = { vAngle: vRandAngle + vRandPhase * 90, hAngle: hRandAngle + hRandPhase * 90 };
        if (applet.options.sample == true || index != correctIndexOne || index != correctIndexTwo) {
            applet.options.body = correctShape;


        }
        if (index == correctIndexOne) {
            applet.options.body = shapeOne;
        }
        if (index == correctIndexTwo) {
            applet.options.body = shapeTwo;
        }

        if (applet.options.sample == true) {
            applet.options.camPos.vAngle = applet.options.camPos.vAngle % 90;
            applet.options.lookAround = false;
            element.setAttribute("style", "position: relative;border: 1px solid #dfdfdf;display: block;width: 350px;margin: 0 auto 15px;overflow: hidden;");
        }

        if (document.location.pathname.includes("review") && !document.location.pathname.includes("preview")) {
            document.getElementById("showInReview").style.display = "block";
            document.getElementById("exerciseText").innerHTML = "A pipával jelölt ábrák azonos testet ábrázolnak, mint a fent lévő.";
            if (applet.options.sample == true) {
                applet.options.lookAround = true;
            }

            if (applet.options.sample != true && index != correctIndexOne && index != correctIndexTwo) {
                let pipa = document.createElement("div");
                pipa.setAttribute("style", "box-shadow:none;color: #053400;position: absolute;margin-left: 20px;margin-top: 20px;width: 30px;height: 15px;border-bottom: solid 7px currentColor;border-left: solid 7px currentColor;-webkit-transform: rotate(-45deg);transform: rotate(-45deg);z-index: 10;box-shadow: 0;")
                element.append(pipa);
            } else if (index == correctIndexOne || index == correctIndexTwo) {
                let nemjo = document.createElement("div");
                nemjo.setAttribute("style", "box-shadow: none;width: 27px;height: 27px;position: absolute;top: 15px;left: 15px;z-index: 1;background-image: url(\"data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' xml:space='preserve'  viewBox='0 0 461 461'%3E%3Cpath fill='%23880000' d='M285 230 456 59c6-6 6-16 0-22L424 5a16 16 0 0 0-22 0L230 176 59 5a16 16 0 0 0-22 0L5 37c-7 6-7 16 0 22l171 171L5 402c-6 6-6 15 0 21l32 33a16 16 0 0 0 22 0l171-171 172 171a16 16 0 0 0 21 0l33-33c6-6 6-15 0-21L285 230z'/%3E%3C/svg%3E\");")
                element.append(nemjo);
            }
            parentElement.parentElement.querySelector(".outcome").style.display = "none";
        }


        if (!(document.location.pathname.includes("review") && !document.location.pathname.includes("preview"))) {
            parentElement.addEventListener('mouseup', checktest);
        }
        let newApplet = new CubeApplet(element, applet.options);
        let checkboxBoolArr = [true, true, true, true];
        checkboxBoolArr[correctIndexOne - 1] = false;
        checkboxBoolArr[correctIndexTwo - 1] = false;

        newApplet.solution = checkboxBoolArr;
        appletArr.push(newApplet);



        if (index == correctIndexTwo) {
            applet.options.body = shapeTwo;
            element.getElementsByTagName("canvas")[0].setAttribute("style", "transform: rotateY(180deg)")
        }


    }

    function evaluateApp(solution, appid) {

        let results = [];
        results[0] = document.getElementById("checkbox0" + appid).checked;
        results[1] = document.getElementById("checkbox1" + appid).checked;
        results[2] = document.getElementById("checkbox2" + appid).checked;
        results[3] = document.getElementById("checkbox3" + appid).checked;
        for (let index = 0; index < 4; index++) {
            if (solution[index] != results[index]) {
                return false;
            }
        }
        return true;
    }

    function checktest(e) {
        let clickedElement = e.path[0];
        if (clickedElement.tagName == "CANVAS") {
            clickedElement = clickedElement.parentElement.parentElement;
        } else if (clickedElement.tagName != "LABEL") {
            return false;
        }

        let appElement = clickedElement.parentElement.parentElement.parentElement.querySelector(".appid");
        let appid = appElement.getAttribute("id");
        let element;
        for (let i = 0; i < appletArr.length; i++) {
            if (appletArr[i].id == appid) {
                element = appletArr[i];
                break;
            }
        }

        setTimeout(() => {

            let results = [];
            results[0] = document.getElementById("checkbox0" + appid).checked;
            results[1] = document.getElementById("checkbox1" + appid).checked;
            results[2] = document.getElementById("checkbox2" + appid).checked;
            results[3] = document.getElementById("checkbox3" + appid).checked;

            let questionState = { correct: 0, result: results };
            let truefalseswitch = clickedElement.parentElement.parentElement.parentElement.querySelector("input." + appid);

            questionState.correct = evaluateApp(element.solution, appid) ? 1 : 0;
            truefalseswitch.value = JSON.stringify(questionState);
        }, "100");



    }

});