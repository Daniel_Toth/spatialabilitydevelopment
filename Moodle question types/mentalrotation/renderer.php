<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * True-false question renderer class.
 *
 * @package    qtype
 * @subpackage mentalrotation
 * @copyright  2009 The Open University
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */


defined('MOODLE_INTERNAL') || die();


/**
 * Generates the output for true-false questions.
 *
 * @copyright  2009 The Open University
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class qtype_mentalrotation_renderer extends qtype_renderer {

    
    public function formulation_and_controls(question_attempt $qa,
            question_display_options $options) {
        global $PAGE, $CFG;

       
        //exit();

        echo "<script src=" . $CFG->wwwroot. '/question/type/mentalrotation/scripts/three.min.js' . "></script>";
        echo "<script src=" . $CFG->wwwroot. '/question/type/mentalrotation/scripts/builder.js' . "></script>";
        echo "<style>
        
        .formulation > input:checked + label.appid > div {
            box-shadow: 0 0 0px 4px #00ab00;
        }
        .formulation > input:checked + label.appid:after {
            content: \"\";
            position: absolute;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background: rgb(0 255 0 / 10%);  
            box-shadow: inset 0 0 20px 10px rgb(0 171 0 / 50%);            
        }
        </style>";
      //  $PAGE->requires->js(new moodle_url($CFG->wwwroot. '/question/type/mentalrotation/scripts/three.min.js'));
       // $PAGE->requires->js(new moodle_url($CFG->wwwroot. '/question/type/mentalrotation/scripts/builder.js'));
        $PAGE->requires->js(new moodle_url($CFG->wwwroot. '/question/type/mentalrotation/d.js'));
        $question = $qa->get_question();
        $response = $qa->get_last_qt_var('answer', '');
        $responseEncoded = json_decode($response);

        $inputname = $qa->get_qt_field_name('answer');
        $trueattributes = array(
            'type' => 'hidden',
            'name' => $inputname,
            'value' => $response,
            'id' => $inputname . 'true',
        );
      /*  $falseattributes = array(
            'type' => 'radio',
            'name' => $inputname,
            'value' => 0,
            'id' => $inputname . 'false',
        );*/

        if ($options->readonly) {
            $trueattributes['disabled'] = 'disabled';
            $falseattributes['disabled'] = 'disabled';
        }
/*
        // Work out which radio button to select (if any).
        $truechecked = false;
        $falsechecked = false;
        $responsearray = array();
        if ($response) {
            $trueattributes['checked'] = 'checked';
            $truechecked = true;
            $responsearray = array('answer' => 1);
        } else if ($response !== '') {
            $falseattributes['checked'] = 'checked';
            $falsechecked = true;
            $responsearray = array('answer' => 1);
        }
*/
        // Work out visual feedback for answer correctness.
        $trueclass = '';
        $falseclass = '';
        $truefeedbackimg = '';
        $falsefeedbackimg = '';
        if ($options->correctness) {
            if ($truechecked) {
                $trueclass = ' ' . $this->feedback_class((int) $question->rightanswer);
                $truefeedbackimg = $this->feedback_image((int) $question->rightanswer);
            } else if ($falsechecked) {
                $falseclass = ' ' . $this->feedback_class((int) (!$question->rightanswer));
                $falsefeedbackimg = $this->feedback_image((int) (!$question->rightanswer));
            }
        }

        /*$radiotrue = html_writer::empty_tag('input', $trueattributes) .
                html_writer::tag('label', get_string('true', 'qtype_mentalrotation'),
                array('for' => $trueattributes['id'], 'class' => 'ml-1'));*/
        $elementid = "randomid" . bin2hex(random_bytes(10));
        $trueattributes["class"] .= $elementid;
        $radiotrue = html_writer::empty_tag('input', $trueattributes);

        $radiofalse = html_writer::empty_tag('input', $falseattributes) .
                html_writer::tag('label', get_string('false', 'qtype_mentalrotation'),
                array('for' => $falseattributes['id'], 'class' => 'ml-1'));

        $questionData = json_decode($question->truefeedback);
        
        $axisColors = ["XPOSITIVEVIEW", "XNEGATIVEVIEW", "YPOSITIVEVIEW", "YNEGATIVEVIEW","ZPOSITIVEVIEW", "ZNEGATIVEVIEW"];
        /*
        if ($response != "") {
            $result = "<div id=\"".$elementid."alredysolved\" class=\"alredysolved\">A feladatot már egyszer megoldottad és az eredménye el lett mentve. $response Ha szeretnéd  újra megoldhatod, vagy lapozz egy másik feladatra.</div>";

        }*/
        $result .= "<h3 id=\"showInReview\" style=\"display: none\">Az ábra az egérrel forgatható</h3><div id=\"" . $elementid . "\" class=\"appid\"></div>";
       // $result = "<div id=\"chooseBox\">";

      /* var_dump($responseEncoded);
       exit();*/

       $result .= "<div style=\"clear:both\"></div><h2 id=\"exerciseText\">Válaszd ki azt a két ábrát, amelyek azonos testet ábrázolnak, mint a fent lévő!</h2>";
        for ($i = 0; $i < 4; $i++) {
                $checked = $responseEncoded->result[$i] ? "checked" : "";
                $result .= "<input id=\"checkbox$i$elementid\" style=\"display:none\" type=\"checkbox\" $checked></input><label for=\"checkbox$i$elementid\" id=\"".$axisColors[$i]. $elementid . "\" class=\"appid\"></label>";
        }
       // $result = "<div id=\"chooseBox\">";
        $result .= "<script>
        let APPSEED = $question->timecreated;
        const SOLUTION = \"" . $questionData->bodyJSON . "\"
        mentalrotationImagePath  = typeof(mentalrotationImagePath) == 'undefined' ? \"" . $CFG->wwwroot. "/question/type/mentalrotation/pix/\" : mentalrotationImagePath;
        appletList = typeof(appletList) == 'undefined' ? [] : appletList;
        appletList.push({
            containerElementName: \"" . $elementid . "\",
            bodyJSON: SOLUTION,
            options: {
                id: \"" . $elementid . "\",
                width: 350,
                height: 350,
                cameraDistance: 25,
                defaultZoom: 2,
                projection:ORTHOGRAPHIC,
                displayMode: Display.TextBook,
                lookAround: true,
                aimBlock: false,
                zoomable: true,
                construction: false,
                limits: {xMin: -7,xMax: 7,yMin: -7,yMax: 7,zMin: -7,zMax: 7},
                camPos: STANDARDVIEW,
                sample: true,
        }
        });";

        for ($i = 0; $i < 4; $i++) {
            $result .= "
            appletList.push({
                containerElementName: \"" . $axisColors[$i]. $elementid . "\",
                bodyJSON: SOLUTION,
                options: {
                    id: \"" . $axisColors[$i]. $elementid . "\",
                    width: 240,
                    height: 240,
                    cameraDistance: 20,
                    defaultZoom: 2.1,
                    projection:ORTHOGRAPHIC,
                    displayMode:  Display.TextBook,
                    lookAround: false,
                    aimBlock: false,
                    zoomable: false,
                    construction: false,
                    limits: {xMin: -7,xMax: 7,yMin: -7,yMax: 7,zMin: -7,zMax: 7},
                    camPos: STANDARDVIEW,
            }
            });";
        }

        $result .= "</script>";
        
       /* $result .= html_writer::tag('div',$question->truefeedback,
                array('class' => 'qtext'));*/

        $result .= html_writer::start_tag('div', array('class' => 'ablock'));
       /* $result .= html_writer::tag('div', get_string('selectone', 'qtype_mentalrotation'),
                array('class' => 'prompt'));*/

        $result .= html_writer::start_tag('div', array('class' => 'answer'));

        $result .= html_writer::tag('div', $radiotrue . ' ' . $truefeedbackimg,array('class' => 'r0' . $trueclass));


       /* $result .= html_writer::tag('div', $radiofalse . ' ' . $falsefeedbackimg,
                array('class' => 'r1' . $falseclass));*/
        $result .= html_writer::end_tag('div'); // Answer.

        $result .= html_writer::end_tag('div'); // Ablock.

        if ($qa->get_state() == question_state::$invalid) {
            $result .= html_writer::nonempty_tag('div',
                    $question->get_validation_error($responsearray),
                    array('class' => 'validationerror'));
        }

        return $result;
    }

    public function specific_feedback(question_attempt $qa) {
        $question = $qa->get_question();
        $response = $qa->get_last_qt_var('answer', '');

        if ($response) {
            return $question->format_text($question->truefeedback, $question->truefeedbackformat,
                    $qa, 'question', 'answerfeedback', $question->trueanswerid);
        } else if ($response !== '') {
            return $question->format_text($question->falsefeedback, $question->falsefeedbackformat,
                    $qa, 'question', 'answerfeedback', $question->falseanswerid);
        }
    }

    public function correct_response(question_attempt $qa) {
        $question = $qa->get_question();

        if ($question->rightanswer) {
            return get_string('correctanswertrue', 'qtype_mentalrotation');
        } else {
            return get_string('correctanswerfalse', 'qtype_mentalrotation');
        }
    }
}
