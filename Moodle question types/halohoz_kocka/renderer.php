<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * True-false question renderer class.
 *
 * @package    qtype
 * @subpackage halohoz_kocka
 * @copyright  2009 The Open University
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */


defined('MOODLE_INTERNAL') || die();


/**
 * Generates the output for true-false questions.
 *
 * @copyright  2009 The Open University
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class qtype_halohoz_kocka_renderer extends qtype_renderer {

    
    public function formulation_and_controls(question_attempt $qa,
            question_display_options $options) {
        global $PAGE, $CFG;

       
        //exit();

        echo "<script src=" . $CFG->wwwroot. '/question/type/halohoz_kocka/scripts/three.min.js' . "></script>";
        echo "<script src=" . $CFG->wwwroot. '/question/type/halohoz_kocka/scripts/builder.js' . "></script>";
        echo "<script src=" . $CFG->wwwroot. '/question/type/halohoz_kocka/scripts/deployggb.js' . "></script>";
        echo "<style>
        
        .formulation > input:checked + label.appid > div {
            box-shadow: 0 0 0px 4px #00ab00;
        }
        .formulation > input:checked + label.appid:after {
            content: \"\";
            box-shadow: none;
            color: #053400;
            position: absolute;
            margin-right: 20px;
            margin-top: 20px;
            width: 30px;
            height: 15px;
            border-bottom: solid 7px currentColor;
            border-left: solid 7px currentColor;
            -webkit-transform: rotate(-45deg);
            transform: rotate(-45deg);
            pointer-events: none;
            z-index: 10;
            top: 0;
            right: 0;
        }
        </style>";
      //  $PAGE->requires->js(new moodle_url($CFG->wwwroot. '/question/type/halohoz_kocka/scripts/three.min.js'));
       // $PAGE->requires->js(new moodle_url($CFG->wwwroot. '/question/type/halohoz_kocka/scripts/builder.js'));
        $PAGE->requires->js(new moodle_url($CFG->wwwroot. '/question/type/halohoz_kocka/d.js'));
        $question = $qa->get_question();
        $response = $qa->get_last_qt_var('answer', '');
        $responseEncoded = json_decode($response);

        $inputname = $qa->get_qt_field_name('answer');
        $trueattributes = array(
            'type' => 'hidden',
            'name' => $inputname,
            'value' => $response,
            'id' => $inputname . 'true',
        );
      /*  $falseattributes = array(
            'type' => 'radio',
            'name' => $inputname,
            'value' => 0,
            'id' => $inputname . 'false',
        );*/

        if ($options->readonly) {
            $trueattributes['disabled'] = 'disabled';
            $falseattributes['disabled'] = 'disabled';
        }
/*
        // Work out which radio button to select (if any).
        $truechecked = false;
        $falsechecked = false;
        $responsearray = array();
        if ($response) {
            $trueattributes['checked'] = 'checked';
            $truechecked = true;
            $responsearray = array('answer' => 1);
        } else if ($response !== '') {
            $falseattributes['checked'] = 'checked';
            $falsechecked = true;
            $responsearray = array('answer' => 1);
        }
*/
        // Work out visual feedback for answer correctness.
        $trueclass = '';
        $falseclass = '';
        $truefeedbackimg = '';
        $falsefeedbackimg = '';
        /*if ($options->correctness) {
            if ($truechecked) {
                $trueclass = ' ' . $this->feedback_class((int) $question->rightanswer);
                $truefeedbackimg = $this->feedback_image((int) $question->rightanswer);
            } else if ($falsechecked) {
                $falseclass = ' ' . $this->feedback_class((int) (!$question->rightanswer));
                $falsefeedbackimg = $this->feedback_image((int) (!$question->rightanswer));
            }
        }*/

        /*$radiotrue = html_writer::empty_tag('input', $trueattributes) .
                html_writer::tag('label', get_string('true', 'qtype_halohoz_kocka'),
                array('for' => $trueattributes['id'], 'class' => 'ml-1'));*/
        $elementid = "randomid" . bin2hex(random_bytes(10));
        $trueattributes["class"] .= $elementid;
        $radiotrue = html_writer::empty_tag('input', $trueattributes);

        $radiofalse = html_writer::empty_tag('input', $falseattributes) .
                html_writer::tag('label', get_string('false', 'qtype_halohoz_kocka'),
                array('for' => $falseattributes['id'], 'class' => 'ml-1'));

        $questionData = json_decode($question->truefeedback);
        
        $axisColors = ["XPOSITIVEVIEW", "XNEGATIVEVIEW", "YPOSITIVEVIEW", "YNEGATIVEVIEW","ZPOSITIVEVIEW", "ZNEGATIVEVIEW"];
        /*
        if ($response != "") {
            $result = "<div id=\"".$elementid."alredysolved\" class=\"alredysolved\">A feladatot már egyszer megoldottad és az eredménye el lett mentve. $response Ha szeretnéd  újra megoldhatod, vagy lapozz egy másik feladatra.</div>";

        }*/
        $result .= "<h2>Válaszd ki a hálónak megfelelő kockát</h2><span>(1 helyes megoldás van)</span><br>";
        $result .= "<h3>Segítség a háló elképzeléséhez</h3><div  style=\"margin: 30px auto\" id=\"ggb-element\"></div><h3>A kocka hálója</h3><div id=\"tablePlan\" style=\"margin: 30px auto\"></div><div><h3>Válaszd ki a megfelelő kockát!</h3><span>(Az ábrák egérrel forgathatóak)</span></div>";

       // $result = "<div id=\"chooseBox\">";

      /* var_dump($responseEncoded);
       exit();*/
        for ($i = 0; $i < 4; $i++) {
                $checked = $responseEncoded->result[$i] ? "checked" : "";
                $result .= "<input id=\"checkbox$i\" style=\"display:none\" name=\"drone\" type=\"radio\" $checked></input><label for=\"checkbox$i\" id=\"".$axisColors[$i]. $elementid . "\" class=\"appid\"></label>";
                if ($i % 2 == 1) {
                    //$result .= "<br style=\"clear:both\">";
                }
        }
       // $result = "<div id=\"chooseBox\">";
      $result .= "<script>
        let APPSEED = " . $question->timecreated .";
        const QUESTIONPATH = \"" . $CFG->wwwroot. "/question/type/halohoz_kocka/\";
        const SOLUTION = \"" . $questionData->bodyJSON . "\";
        halohoz_kockaImagePath  = typeof(halohoz_kockaImagePath) == 'undefined' ? \"" . $CFG->wwwroot. "/question/type/halohoz_kocka/pix/\" : halohoz_kockaImagePath;
        appletList = typeof(appletList) == 'undefined' ? [] : appletList;";

        for ($i = 0; $i < 4; $i++) {
            $result .= "
            appletList.push({
                containerElementName: \"" . $axisColors[$i]. $elementid . "\",
                bodyJSON: SOLUTION,
                options: {
                    id: \"" . $axisColors[$i]. $elementid . "\",
                    width: 160,
                    height: 160,
                    cameraDistance: 20,
                    defaultZoom: 15,
                    projection:PERSPECTIVE,
                    displayMode:  Display.TextBook,
                    lookAround: true,
                    aimBlock: false,
                    zoomable: true,
                    construction: false,
                    limits: {xMin: -2,xMax: 2,yMin: -2,yMax: 2,zMin: 0,zMax: 5},
                    camPos: STANDARDVIEW,
            }
            });";
        }

        $result .= "
        
        var params = {  \"appName\": \"classic\",
            \"showToolBar\": false,
            \"showAlgebraInput\": false,
            \"showMenuBar\": false ,
            \"filename\": QUESTIONPATH + \"kockahaloi12.ggb\",
            \"width\": 450,
            \"height\": 450,
            \"enableRightClick\": false,
            \"showResetIcon\": false,
            \"useBrowserForJS\": true,
};
        var applet = new GGBApplet(params, true);
        
        applet.setHTML5Codebase(QUESTIONPATH + 'HTML5/5.0/web3d/');
        applet.inject('ggb-element');
        </script>";
        
       /* $result .= html_writer::tag('div',$question->truefeedback,
                array('class' => 'qtext'));*/

        $result .= html_writer::start_tag('div', array('class' => 'ablock'));
       /* $result .= html_writer::tag('div', get_string('selectone', 'qtype_halohoz_kocka'),
                array('class' => 'prompt'));*/

        $result .= html_writer::start_tag('div', array('class' => 'answer'));

        $result .= html_writer::tag('div', $radiotrue . ' ' . $truefeedbackimg,array('class' => 'truefalseswitch'));


       /* $result .= html_writer::tag('div', $radiofalse . ' ' . $falsefeedbackimg,
                array('class' => 'r1' . $falseclass));*/
        $result .= html_writer::end_tag('div'); // Answer.

        $result .= html_writer::end_tag('div'); // Ablock.

        if ($qa->get_state() == question_state::$invalid) {
            $result .= html_writer::nonempty_tag('div',
                    $question->get_validation_error($responsearray),
                    array('class' => 'validationerror'));
        }

        return $result;
    }

    public function specific_feedback(question_attempt $qa) {
        $question = $qa->get_question();
        $response = $qa->get_last_qt_var('answer', '');

        if ($response) {
            return $question->format_text($question->truefeedback, $question->truefeedbackformat,
                    $qa, 'question', 'answerfeedback', $question->trueanswerid);
        } else if ($response !== '') {
            return $question->format_text($question->falsefeedback, $question->falsefeedbackformat,
                    $qa, 'question', 'answerfeedback', $question->falseanswerid);
        }
    }

    public function correct_response(question_attempt $qa) {
        $question = $qa->get_question();

        if ($question->rightanswer) {
            return get_string('correctanswertrue', 'qtype_halohoz_kocka');
        } else {
            return get_string('correctanswerfalse', 'qtype_halohoz_kocka');
        }
    }
}
