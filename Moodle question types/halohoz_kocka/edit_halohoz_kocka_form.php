<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Defines the editing form for the true-false question type.
 *
 * @package    qtype
 * @subpackage halohoz_kocka
 * @copyright  2007 Jamie Pratt
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */


defined('MOODLE_INTERNAL') || die();

 require_once($CFG->dirroot.'/question/type/edit_question_form.php');
/**
 * True-false question editing form definition.
 *
 * @copyright  2007 Jamie Pratt
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */


class qtype_halohoz_kocka_edit_form extends question_edit_form {
    /**p
     * Add question-type specific form fields.
     *
     * @param object $mform the form being built.
     */
    protected function definition_inner($mform) {
        global $PAGE, $CFG;
        
        echo "<script>var halohoz_kockaImagePath  = \"" . $CFG->wwwroot. "/question/type/halohoz_kocka/pix/\";</script>";
        $PAGE->requires->js(new moodle_url($CFG->wwwroot. '/question/type/halohoz_kocka/scripts/three.min.js'),true);
        $PAGE->requires->js(new moodle_url($CFG->wwwroot. '/question/type/halohoz_kocka/scripts/builder.js'),true);
        $PAGE->requires->js(new moodle_url($CFG->wwwroot. '/question/type/halohoz_kocka/scripts/questionedit.js'), true);
        $PAGE->requires->css(new moodle_url($CFG->wwwroot. '/question/type/halohoz_kocka/styles.css'));
        
        $mform->addElement('hidden', 'correctanswer', 1);
       /* $mform->addElement('select', 'correctanswer',
        get_string('correctanswer', 'qtype_halohoz_kocka'), array(
        0 => get_string('false', 'qtype_halohoz_kocka'),
        1 => get_string('true', 'qtype_halohoz_kocka')));*/
        /*

        $mform->addElement('text', 'body', "Body (JSON)",
                array('size' => 50, 'maxlength' => 255));
        $mform->setType('body', PARAM_TEXT);
        $mform->addRule('body', null, 'required', null, 'client');*/

        $mform->addElement('text', 'feedbacktrue',
        "Body (JSON)", array('rows' => 10),array('size' => 50, 'maxlength' => 255));
        $mform->setType('body', PARAM_TEXT);

/*
        $output = "<script>let halohoz_kockaImagePath  = \"" . $CFG->wwwroot. "/question/type/halohoz_kocka/pix/\";</script>";
        $mform->addElement('html', $output);
*/

        //$mform->addRule('body', null, 'required', null, 'client');
/*
        
        $mform->addElement('editor', 'feedbackfalse',
                get_string('feedbackfalse', 'qtype_halohoz_kocka'), array('rows' => 10), $this->editoroptions);
        $mform->setType('feedbackfalse', PARAM_RAW);
*/

        $mform->addElement('hidden', 'feedbackfalse',
        "", array('rows' => 10),array('size' => 50, 'maxlength' => 255));
        $mform->setType('feedbackfalse', PARAM_TEXT);

/*
        $mform->addElement('checkbox', 'xpos', "<img style=\"width: 50px; height: 50px;transform: rotate(270deg);\" src=\"" . $CFG->wwwroot. "/question/type/halohoz_kocka/pix/blueturtle.svg\"/>");
        $mform->addElement('checkbox', 'xneg', "<img style=\"width: 50px; height: 50px;transform: rotate(270deg);\" src=\"" . $CFG->wwwroot. "/question/type/halohoz_kocka/pix/greenturtle.svg\"/>");
        $mform->addElement('checkbox', 'ypos', "<img style=\"width: 50px; height: 50px;transform: rotate(270deg);\" src=\"" . $CFG->wwwroot. "/question/type/halohoz_kocka/pix/orangeturtle.svg\"/>");
        $mform->addElement('checkbox', 'yneg', "<img style=\"width: 50px; height: 50px;transform: rotate(270deg);\" src=\"" . $CFG->wwwroot. "/question/type/halohoz_kocka/pix/redturtle.svg\"/>");
        $mform->addElement('checkbox', 'zpos', "<img style=\"width: 50px; height: 50px;transform: rotate(270deg);\" src=\"" . $CFG->wwwroot. "/question/type/halohoz_kocka/pix/whiteturtle.svg\"/>");
        $mform->addElement('checkbox', 'zneg', "<img style=\"width: 50px; height: 50px;transform: rotate(270deg);\" src=\"" . $CFG->wwwroot. "/question/type/halohoz_kocka/pix/yellowturtle.svg\"/>");


*/
       /* $mform->addElement('header', 'multitriesheader',
                get_string('settingsformultipletries', 'question'));*/

        $mform->addElement('hidden', 'penalty', 1);
        $mform->setType('penalty', PARAM_FLOAT);

        $mform->addElement('hidden', 'penaltymessage', 1);


       // $mform->addHelpButton('penaltymessage', 'penaltyforeachincorrecttry', 'question');
    }

    public function data_preprocessing($question) {
       // var_dump( $question);

        $question = parent::data_preprocessing($question);

        if (!empty($question->options->trueanswer)) {
            $trueanswer = $question->options->answers[$question->options->trueanswer];
            $question->correctanswer = ($trueanswer->fraction != 0);

            $draftid = file_get_submitted_draft_itemid('trueanswer');
            $answerid = $question->options->trueanswer;

            $question->feedbacktrue = array();
            $question->feedbacktrue['format'] = $trueanswer->feedbackformat;
            $question->feedbacktrue['text'] = file_prepare_draft_area(
                $draftid,             // Draftid
                $this->context->id,   // context
                'question',           // component
                'answerfeedback',     // filarea
                !empty($answerid) ? (int) $answerid : null, // itemid
                $this->fileoptions,   // options
                $trueanswer->feedback // text.
            );
            $question->feedbacktrue['itemid'] = $draftid;
        }

        if (!empty($question->options->falseanswer)) {
            $falseanswer = $question->options->answers[$question->options->falseanswer];

            $draftid = file_get_submitted_draft_itemid('falseanswer');
            $answerid = $question->options->falseanswer;

            $question->feedbackfalse = array();
            $question->feedbackfalse['format'] = $falseanswer->feedbackformat;
            $question->feedbackfalse['text'] = file_prepare_draft_area(
                $draftid,              // Draftid
                $this->context->id,    // context
                'question',            // component
                'answerfeedback',      // filarea
                !empty($answerid) ? (int) $answerid : null, // itemid
                $this->fileoptions,    // options
                $falseanswer->feedback // text.
            );
            $question->feedbackfalse['itemid'] = $draftid;
        }

        return $question;
    }

    public function qtype() {
        return 'halohoz_kocka';
    }
}
