let halloIndex;
function onAdd(name){
    if (name != "b") {
        ggbApplet.evalCommand("SetConditionToShowObject("+name+", false)"); 
    }
}

function ggbOnInit(name, api){
    ggbApplet.evalCommand("t="+(halloIndex + 1)); 
    
    api.registerAddListener("onAdd");
}
window.addEventListener("load", (event) => { 

    if (document.location.pathname.includes("review") && !document.location.pathname.includes("preview")) {
        if (!window.location.href.includes("showall=")) {
            window.location.href += "&showall=0";
        }
    }

    function rand(a) {
          var t = a += 0x6D2B79F5;
          t = Math.imul(t ^ t >>> 15, t | 1);
          t ^= t + Math.imul(t ^ t >>> 7, t | 61);
          return ((t ^ t >>> 14) >>> 0) / 4294967296;
    }

    function getRandomExcept(l,h,e,seed) {
        if (l == h && h == e) {
            return e;
        }
        let t;
        do {
            t = Math.floor(rand(seed++)*(h - l + 1) + l);
        } while (t == e);
        return t;
    }

    function shuffle(array) {
        let currentIndex = array.length,  randomIndex;
      
        // While there remain elements to shuffle.
        while (currentIndex != 0) {
      
          // Pick a remaining element.
          randomIndex = getRandomExcept(0,currentIndex-1,1000,APPSEED);
          APPSEED += 97;
          currentIndex--;
      
          // And swap it with the current element.
          [array[currentIndex], array[randomIndex]] = [array[randomIndex], array[currentIndex]];
        }
      
        return array;
      }

    function rotLeft(a, d) {
    d = d % a.length;
    if (d === 0) return a;
    return a.concat(a.splice(0, d));
    }


    function generatePlan(limits) {
        

        let map = Array(25);
        let index = 0;


        for (let c = 0; c<25;c++) {
            map[c] = 0;
        }

        let ideal = [0,3,2,1,0,0];
        for (let c = 1; c <=5; c++) {
            let fill =  getRandomExcept(0,1,100,APPSEED) + ideal[c];
            APPSEED += 37;
            for (let q=0;(index<25 && q<fill); q++) {
                map[index] = c;
                index++;
            }
        }



        let assign = [12,7,8,13,18,17,16,11,6,1,2,3,4,9,14,19,24,23,22,21,20,15,10,5,0];

        if ( getRandomExcept(0,1,100,APPSEED) == 1) {
            assign = [12,7,6,11,16,17,18,13,8,3,2,1,0,5,10,15,20,21,22,23,24,19,14,9,4];
        }
        APPSEED++;
        map = map.sort().reverse();;
        let p = map.slice();
        for (let i = 0; i < p.length; i++) {
            map[assign[i]] = p[i];
            
        }

        for (let i = 0; i<7; i++) {
            let di;
            do {
                di = getRandomExcept(0,24,100,APPSEED);
                APPSEED++;
            } while (map[di] == 5);

            map[di]++;
        }
        for (let i = 0; i<10; i++) {
            let di;
            do {
                di = getRandomExcept(0,24,100,APPSEED);
                APPSEED++;
            } while (map[di] == 0);

            map[di]--;
        }

        let cubecounter = 0;
        for (let w = 0; w < map.length; w++) {
            cubecounter = cubecounter + map[w];
        }

        //alert(cubecounter);

        if (cubecounter < 20) {

            for (let i = 0; i<(20- cubecounter); i++) {
                let di;
                do {
                    di = getRandomExcept(0,24,100,APPSEED);
                    APPSEED++;
                } while (map[di] == 5);
    
                map[di]++;
            }

        }
        return map;    
    }


    function mirrorPlan(map) {
        APPSEED++;
        let assign = [20,21,22,23,24,15,16,17,18,19,10,11,12,13,14,5,6,7,8,9,0,1,2,3,4];

        if ( getRandomExcept(0,1,100,APPSEED) == 1) {
            assign = [4,3,2,1,0,9,8,7,6,5,14,13,12,11,10,19,18,17,16,15,24,23,22,21,20];
        }
        let p = map.slice();
        for (let i = 0; i < p.length; i++) {
            p[assign[i]] = map[i];
            
        }

        return p;
    }


    function shiftPlan(map) {
        APPSEED++;
        let assign = [1,2,3,4,0,6,7,8,9,5,11,12,13,14,10,16,17,18,19,15,21,22,23,24,20];

        if ( getRandomExcept(0,1,100,APPSEED) == 1) {
            assign = [5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,0,1,2,3,4];
        }
        let p = map.slice();
        for (let i = 0; i < p.length; i++) {
            p[assign[i]] = map[i];
            
        }

        return p;
    }


    function swapPlan(map) {
        APPSEED++;
        let p = map.slice();
        let maxIndex = 0;
        let swapIndex = 0;
        for (let index = 0; index < p.length; index++) {
            if (p[index] > p[maxIndex]) {
                maxIndex = index;
            }
            
        }

        let assign = [12,7,8,13,18,17,16,11,6,1,2,3,4,9,14,19,24,23,22,21,20,15,10,5,0];


        for (let index = 0; index < p.length; index++) {
            if (p[assign[index]] != p[maxIndex]) {
                swapIndex = assign[index];
                break;
            }
        }


        [p[swapIndex], p[maxIndex]] = [p[maxIndex], p[swapIndex]];
/*
        for (let i = 0; i<1; i++) {
            let di;
            do {
                di = getRandomExcept(0,24,100,APPSEED);
                APPSEED++;
            } while (p[di] == 5);

            p[di]++;
        }
        for (let i = 0; i<2; i++) {
            let di;
            do {
                di = getRandomExcept(0,24,100,APPSEED);
                APPSEED++;
            } while (p[di] == 0);

            p[di]--;
        }
*/
        return p;
    }


    function createBodyFromPlan(map,limits) {
        let cubeBody = Array();

        let x,y,z;
        for (x=0; x<=limits.xMax-limits.xMin; x++) {
            cubeBody[x] = Array();
            for (y=0; y<=limits.yMax-limits.yMin; y++) {
                cubeBody[x][y] = Array();
                for (z=0; z<=limits.zMax-limits.zMin; z++) {
                    cubeBody[x][y][z] = 0;
                }
            }
        }


        let index = 0;
        for (x=0; x<=limits.xMax-limits.xMin; x++) {
            for (y=0; y<=limits.yMax-limits.yMin; y++) {
                for (z=0; z<map[index];z++) {
                    cubeBody[x][y][z] = 2;
                }
                index++;
                
            }
        }

        return JSON.stringify(cubeBody);
    }


    function rotateArrayElements(arr, indexArr) {
        const rotatedArr = [];
        for (let i = 0; i < indexArr.length; i++) {
          rotatedArr.push(arr[indexArr[i]]);
        }
        return rotatedArr;
      }


    function rot90(p1) {
        let rotM = [4,9,14,19,24,3,8,13,18,23,2,7,12,17,22,1,6,11,16,21,0,5,10,15,20];
        let p2 = p1.slice();
        for (let i = 0; i < p1.length; i++) {
            p2[rotM[i]] = p1[i]
        }
        return p2;
    }



    let answers = shuffle([0,1,2,3]);


    let plan = generatePlan({xMin: -2,xMax: 2,yMin: -2,yMax: 2,zMin: 0,zMax: 5});
    
    let wrongplan1 = mirrorPlan(plan.slice());
    let wrongplan2 = shiftPlan(plan.slice());
    let wrongplan3 = swapPlan(plan.slice());



    
    let sideColors = shuffle(["#ffffff","#ff0000","#0000ff","#ff8000", "#00ff00","#ffff00"]);



   // [0,1,2,3,4,5]




    APPSEED++;
    let wrongSideColors1 = rotateArrayElements(sideColors, [[5,1,2,3,4,0],[0,1,4,3,2,5],[0,3,2,1,4,5]][getRandomExcept(0,2,100,APPSEED)]);
    APPSEED++;
    let wrongSideColors2 = rotateArrayElements(sideColors, [[1,0,2,3,4,5],[2,1,0,3,4,5],[3,1,2,0,4,5],[4,1,2,3,0,5],[0,4,2,3,1,5],[0,2,1,3,4,5],[0,1,3,2,4,5],[0,1,2,4,3,5],[0,5,2,3,4,1],[0,1,5,3,4,2],[0,1,2,5,4,3],[0,1,2,3,5,4]][getRandomExcept(0,11,100,APPSEED)]);
    APPSEED++;
    let wrongSideColors3 = rotateArrayElements(sideColors, [[1,2,0,3,4,5],[2,0,1,3,4,5],[2,1,3,0,4,5],[3,1,0,2,4,5],[3,1,2,4,0,5],[4,1,2,0,3,5],[1,4,2,3,0,5],[4,0,2,3,1,5],[0,1,3,5,4,2],[0,1,5,2,4,3],[0,2,5,3,4,1],[0,5,1,3,4,2],[0,4,2,3,5,1],[0,5,2,3,1,4],[0,1,2,4,5,3],[0,1,2,5,3,4]][getRandomExcept(0,15,100,APPSEED)]);
   /* [0,5][2,4][1,3]*/
    /*[5,1,2,3,4,0] [0,1,4,3,2,5] [0,3,2,1,4,5]*/
/*
    [0,1][0,2][0,3][0,4]   [1,4][1,2][2,3][3,4]  [1,5][2,5][3,5][4,5]
    [0,1,2][0,2,3][0,3,4][0,4,1][0,4,1]*/
    halok = [
        [
        9,9,3,9,9,
        9,4,0,2,9,
        9,9,1,9,9,
        9,9,5,9,9,
        9,9,9,9,9
        ],[
        9,9,3,2,9,
        9,4,0,9,9,
        9,9,1,9,9,
        9,9,5,9,9,
        9,9,9,9,9
        ],[
        9,9,3,9,9,
        9,9,0,2,9,
        9,4,1,9,9,
        9,9,5,9,9,
        9,9,9,9,9
        ],[
        9,9,3,9,9,
        9,4,0,9,9,
        9,9,1,9,9,
        9,9,5,2,9,
        9,9,9,9,9
        ],[
        9,9,9,9,9,
        9,9,0,2,5,
        3,4,1,9,9,
        9,9,9,9,9,
        9,9,9,9,9
        ],[
        9,9,9,9,9,
        9,4,0,2,9,
        9,9,1,9,9,
        9,9,5,9,9,
        9,9,3,9,9
        ],[
        9,9,9,3,9,
        9,4,0,2,9,
        9,9,1,9,9,
        9,9,5,9,9,
        9,9,9,9,9
        ],[
        9,9,3,2,9,
        9,9,0,9,9,
        9,9,1,9,9,
        9,4,5,9,9,
        9,9,9,9,9
        ],[
        9,9,3,2,9,
        9,9,0,9,9,
        9,4,1,9,9,
        9,5,9,9,9,
        9,9,9,9,9
        ],[
        9,9,3,9,9,
        9,9,0,2,9,
        9,4,1,9,9,
        9,5,9,9,9,
        9,9,9,9,9
        ],[
        9,9,9,3,9,
        9,9,0,2,9,
        9,4,1,9,9,
        9,5,9,9,9,
        9,9,9,9,9
        ]


    ,[
        9,4,3,9,9,
        9,9,0,2,9,
        9,9,1,9,9,
        9,9,5,9,9,
        9,9,9,9,9
        ],[
        9,9,3,9,9,
        9,4,0,9,9,
        9,9,1,2,9,
        9,9,5,9,9,
        9,9,9,9,9
        ],[
        9,9,3,9,9,
        9,9,0,2,9,
        9,9,1,9,9,
        9,4,5,9,9,
        9,9,9,9,9
        ],[
        9,9,9,9,9,
        5,4,0,9,9,
        9,9,1,2,3,
        9,9,9,9,9,
        9,9,9,9,9
        ],[
        9,3,9,9,9,
        9,4,0,2,9,
        9,9,1,9,9,
        9,9,5,9,9,
        9,9,9,9,9
        ],[
        9,9,3,2,9,
        9,9,0,9,9,
        9,9,1,9,9,
        9,4,5,9,9,
        9,9,9,9,9
        ],[
        9,4,3,9,9,
        9,9,0,9,9,
        9,9,1,2,9,
        9,9,9,5,9,
        9,9,9,9,9
        ],[
        9,9,3,9,9,
        9,4,0,9,9,
        9,9,1,2,9,
        9,9,9,5,9,
        9,9,9,9,9
        ],[
        9,3,9,9,9,
        9,4,0,9,9,
        9,9,1,2,9,
        9,9,9,5,9,
        9,9,9,9,9
        ]


    ]


    let cubeColors = shuffle([0,1,2,3,4,5]);
    APPSEED++;
    halloIndex = getRandomExcept(0,19,100,APPSEED);
    
    APPSEED++;
    let rotationNumber = getRandomExcept(0,3,100,APPSEED);
    var originalSeed = APPSEED;
    let appletArr = [];
    let containerElement = document.getElementById("tablePlan");
    let exerciseNet = halok[halloIndex];
    for (let c = 0; c < rotationNumber; c++) {
        exerciseNet = rot90(exerciseNet);
    }


    let t = "<table style=\"border: 0px solid #888; background-color: transparent;color: #333;font-family: cursive, verdana, monospace;font-weight: bold;text-align: center;margin: auto\">";
    for (let y=0; y<5; y++) {
        t += "<tr>";
        for (let x=0; x<5; x++) {

            let num = exerciseNet[5*y + x];
            num = num == 9 ? "background-color: transparent" : "border:1px solid #000;background-color: " + sideColors[num];
            t += "<td style=\"width: 42px; height: 42px;" + num + " \"></td>";
        }
        t += "</tr>";
    }
    t += "</table>";

    containerElement.innerHTML = t;

    appletList.forEach(loadApplet);

    
    
    

      // 

    /*
    
    



    */
/*
    var params = {  "appName": "classic",
                    "showToolBar": false,
                    "showAlgebraInput": false,
                    "showMenuBar": false ,
                    "filename": QUESTIONPATH + "kockahaloi7.ggb",
                    "width": 400,
                    "height": 500,
                    "enableRightClick": false,
                    "showResetIcon": false,
                    "useBrowserForJS": true,
                };
var applet = new GGBApplet(params, true);
applet.setHTML5Codebase(QUESTIONPATH + 'HTML5/5.0/web3d/');
applet.inject('ggb-element');*/
/*



*/

  

 
   


    function loadApplet(applet, index) {
        APPSEED++;

        let bodyJSON = applet.bodyJSON;
        let element = document.getElementById(applet.containerElementName);
        let parentElement = element.parentElement;

       // parentElement.addEventListener('mousemove', checktest);
        





        element.setAttribute("style","position: relative;border: 1px;display: inline-block; float: left; margin: 5px;overflow: hidden;");
        parentElement.setAttribute("appid", index);
       
      //  hAngle: getRandomExcept(0,360,1000,APPSEED + 222 + index
      //getRandomExcept(-90,90,100,APPSEED + 111+ index)
        APPSEED += 111;
        let vRandAngle = getRandomExcept(45,45,100,APPSEED + index);
        APPSEED += vRandAngle + 21;
        let vRandPhase = getRandomExcept(0,3,100,APPSEED + index);
        APPSEED += 222;
        let hRandAngle = getRandomExcept(45,45,100,APPSEED + index);
        APPSEED += hRandAngle + 97;
        applet.options.camPos = {vAngle: vRandAngle, hAngle: hRandAngle+(index+(originalSeed % 4))*90};

        
        applet.options.randomRotation = {x: getRandomExcept(0,3,1000,APPSEED + 1) * Math.PI/2,
                                         y: getRandomExcept(0,3,1000,APPSEED + 2)* Math.PI/2,
                                         z: getRandomExcept(0,3,1000,APPSEED + 3) * Math.PI/2};
   
        if (index == answers[0] ) {
            applet.options.colorSides = sideColors;
            
        } else if (index == answers[1] ) {
            applet.options.colorSides = wrongSideColors1;
        } else if (index == answers[2] ) {
            applet.options.colorSides = wrongSideColors2;
        } else {
            applet.options.colorSides = wrongSideColors3;
        }


        


 


        if (document.location.pathname.includes("review") && !document.location.pathname.includes("preview")) {


           


            applet.options.lookAround = true;

            if (index == answers[0]) {
                let pipa = document.createElement("div");

                                            
                pipa.setAttribute("style","pointer-events: none;box-shadow: none;color: #00ff00;position: absolute;margin-left: -15px;margin-top: -15px;width: 30px;height: 15px;border-bottom: solid 7px currentColor;border-left: solid 7px currentColor;-webkit-transform: rotate(-45deg);transform: rotate(-45deg) scale(3);z-index: 10;left: 50%;opacity: 0.8;top: 50%;")
                element.append(pipa);  
            } else {
                let nemjo = document.createElement("div");
                element.append(nemjo);
            }
            parentElement.parentElement.querySelector(".outcome").style.display="none";
        }


        if (!(document.location.pathname.includes("review") && !document.location.pathname.includes("preview"))) {
            parentElement.addEventListener('mouseup', checktest);
        }


        let newApplet = new CubeApplet(element, applet.options);
        appletArr.push(newApplet);
    }

    function evaluateApp(solution, appid) {

        let results = [];
        results[0] = document.getElementById("checkbox0").checked;
        results[1] = document.getElementById("checkbox1").checked;
        results[2] = document.getElementById("checkbox2").checked;
        results[3] = document.getElementById("checkbox3").checked;
        for (let index = 0; index < 4; index++) {
            if (solution[index] != results[index]) {
                return false;
            }            
        }
        return true;
    }
    
    function checktest(e) {

       
        setTimeout(() => {
            let results = [];
            results[0] = document.getElementById("checkbox0").checked;
            results[1] = document.getElementById("checkbox1").checked;
            results[2] = document.getElementById("checkbox2").checked;
            results[3] = document.getElementById("checkbox3").checked;

            let questionState = {correct: 0, result: results};
            let truefalseswitch = document.querySelector(".truefalseswitch > input");
            questionState.correct = results[answers[0]] == true ? 1 : 0;
            truefalseswitch.value = JSON.stringify(questionState);
          }, "100");
    }

});