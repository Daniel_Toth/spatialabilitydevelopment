<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'qtype_tervhez_epitmenyt', language 'en', branch 'MOODLE_20_STABLE'
 *
 * @package    qtype
 * @subpackage tervhez_epitmenyt
 * @copyright  1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['correctanswer'] = 'Helyes válasz';
$string['correctanswerfalse'] = 'The correct answer is \'False\'.';
$string['correctanswertrue'] = 'The correct answer is \'True\'.';
$string['false'] = 'False';
$string['feedbackfalse'] = 'Feedback for the response \'False\'.';
$string['feedbacktrue'] = 'Feedback for the response \'True\'.';
$string['body'] = 'Test JSON';

$string['pleaseselectananswer'] = 'Please select an answer.';
$string['selectone'] = 'Select one:';
$string['true'] = 'True';
$string['pluginname'] = 'Tervhezepitmeny';
$string['pluginname_help'] = 'Valaszd ki, hogy melyik epitmeny epult a terv alapjan!';
$string['pluginname_link'] = 'question/type/tervhez_epitmenyt';
$string['pluginnameadding'] = 'Tervhez epitmenyt kerdes hozzaadasa';
$string['pluginnameediting'] = 'Tervhez epitmenyt kerdes szerkesztese';
$string['pluginnamesummary'] = 'Egy feladattipus amelyben ki kell valasztani, hogy melyik epitmeny epult a terv alapjan.';
$string['privacy:metadata'] = 'A tervhez epitmenyt feladattipus nem ment szemelyes adatokat.';
