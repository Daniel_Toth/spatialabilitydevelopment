window.addEventListener("load", (event) => { 

    if (document.location.pathname.includes("review") && !document.location.pathname.includes("preview")) {
        if (!window.location.href.includes("showall=")) {
            window.location.href += "&showall=0";
        }
    }

    

    function rand(a) {
          var t = a += 0x6D2B79F5;
          t = Math.imul(t ^ t >>> 15, t | 1);
          t ^= t + Math.imul(t ^ t >>> 7, t | 61);
          return ((t ^ t >>> 14) >>> 0) / 4294967296;
    }

    function getRandomExcept(l,h,e,seed) {
        if (l == h && h == e) {
            return e;
        }
        let t;
        do {
            t = Math.floor(rand(seed++)*(h - l + 1) + l);
        } while (t == e);
        return t;
    }

    function shuffle(array) {
        let currentIndex = array.length,  randomIndex;
      
        // While there remain elements to shuffle.
        while (currentIndex != 0) {
      
          // Pick a remaining element.
          randomIndex = getRandomExcept(0,currentIndex-1,1000,APPSEED);
          APPSEED += 97;
          currentIndex--;
      
          // And swap it with the current element.
          [array[currentIndex], array[randomIndex]] = [array[randomIndex], array[currentIndex]];
        }
      
        return array;
      }

    function rotLeft(a, d) {
    d = d % a.length;
    if (d === 0) return a;
    return a.concat(a.splice(0, d));
    }

    function generatePlan(limits) {
        

        let map = Array(25);
        let index = 0;


        for (let c = 0; c<25;c++) {
            map[c] = 0;
        }

        let ideal = [0,3,2,1,0,0];
        for (let c = 1; c <=5; c++) {
            let fill =  getRandomExcept(0,1,100,APPSEED) + ideal[c];
            APPSEED += 37;
            for (let q=0;(index<25 && q<fill); q++) {
                map[index] = c;
                index++;
            }
        }



        let assign = [12,7,8,13,18,17,16,11,6,1,2,3,4,9,14,19,24,23,22,21,20,15,10,5,0];

        if ( getRandomExcept(0,1,100,APPSEED) == 1) {
            assign = [12,7,6,11,16,17,18,13,8,3,2,1,0,5,10,15,20,21,22,23,24,19,14,9,4];
        }
        APPSEED++;
        map = map.sort().reverse();;
        let p = map.slice();
        for (let i = 0; i < p.length; i++) {
            map[assign[i]] = p[i];
            
        }

        for (let i = 0; i<7; i++) {
            let di;
            do {
                di = getRandomExcept(0,24,100,APPSEED);
                APPSEED++;
            } while (map[di] == 5);

            map[di]++;
        }
        for (let i = 0; i<10; i++) {
            let di;
            do {
                di = getRandomExcept(0,24,100,APPSEED);
                APPSEED++;
            } while (map[di] == 0);

            map[di]--;
        }

        let cubecounter = 0;
        for (let w = 0; w < map.length; w++) {
            cubecounter = cubecounter + map[w];
        }

        //alert(cubecounter);

        if (cubecounter < 20) {

            for (let i = 0; i<(20- cubecounter); i++) {
                let di;
                do {
                    di = getRandomExcept(0,24,100,APPSEED);
                    APPSEED++;
                } while (map[di] == 5);
    
                map[di]++;
            }

        }
        return map;    
    }


    function mirrorPlan(map) {
        APPSEED++;
        let assign = [20,21,22,23,24,15,16,17,18,19,10,11,12,13,14,5,6,7,8,9,0,1,2,3,4];

        if ( getRandomExcept(0,1,100,APPSEED) == 1) {
            assign = [4,3,2,1,0,9,8,7,6,5,14,13,12,11,10,19,18,17,16,15,24,23,22,21,20];
        }
        let p = map.slice();
        for (let i = 0; i < p.length; i++) {
            p[assign[i]] = map[i];
            
        }

        return p;
    }


    function shiftPlan(map) {
        APPSEED++;
        let assign = [1,2,3,4,0,6,7,8,9,5,11,12,13,14,10,16,17,18,19,15,21,22,23,24,20];

        if ( getRandomExcept(0,1,100,APPSEED) == 1) {
            assign = [5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,0,1,2,3,4];
        }
        let p = map.slice();
        for (let i = 0; i < p.length; i++) {
            p[assign[i]] = map[i];
            
        }

        return p;
    }


    function swapPlan(map) {
        APPSEED++;
        let p = map.slice();
        let maxIndex = 0;
        let swapIndex = 0;
        for (let index = 0; index < p.length; index++) {
            if (p[index] > p[maxIndex]) {
                maxIndex = index;
            }
            
        }

        let assign = [12,7,8,13,18,17,16,11,6,1,2,3,4,9,14,19,24,23,22,21,20,15,10,5,0];


        for (let index = 0; index < p.length; index++) {
            if (p[assign[index]] != p[maxIndex]) {
                swapIndex = assign[index];
                break;
            }
        }


        [p[swapIndex], p[maxIndex]] = [p[maxIndex], p[swapIndex]];
/*
        for (let i = 0; i<1; i++) {
            let di;
            do {
                di = getRandomExcept(0,24,100,APPSEED);
                APPSEED++;
            } while (p[di] == 5);

            p[di]++;
        }
        for (let i = 0; i<2; i++) {
            let di;
            do {
                di = getRandomExcept(0,24,100,APPSEED);
                APPSEED++;
            } while (p[di] == 0);

            p[di]--;
        }
*/
        return p;
    }


    function createBodyFromPlan(map,limits) {
        let cubeBody = Array();

        let x,y,z;
        for (x=0; x<=limits.xMax-limits.xMin; x++) {
            cubeBody[x] = Array();
            for (y=0; y<=limits.yMax-limits.yMin; y++) {
                cubeBody[x][y] = Array();
                for (z=0; z<=limits.zMax-limits.zMin; z++) {
                    cubeBody[x][y][z] = 0;
                }
            }
        }


        let index = 0;
        for (x=0; x<=limits.xMax-limits.xMin; x++) {
            for (y=0; y<=limits.yMax-limits.yMin; y++) {
                for (z=0; z<map[index];z++) {
                    cubeBody[x][y][z] = getRandomExcept(1,6,5,APPSEED);
                    APPSEED++;
                }
                index++;
                
            }
        }

        return JSON.stringify(cubeBody);
    }


    let answers = shuffle([0,1,2,3]);


    let plan = generatePlan({xMin: -2,xMax: 2,yMin: -2,yMax: 2,zMin: 0,zMax: 5});
    let randomBody = createBodyFromPlan(plan,{xMin: -2,xMax: 2,yMin: -2,yMax: 2,zMin: 0,zMax: 5}) ;

    var originalSeed = APPSEED;

    let nezet = getRandomExcept(0,1,100,APPSEED + 66);
    let appletArr = [];
    appletList.forEach(loadApplet);
    checktest();

    

    function loadApplet(applet, index) {
        APPSEED++;

        let bodyJSON = applet.bodyJSON;
        let element = document.getElementById(applet.containerElementName);
        let parentElement = element.parentElement;

       // parentElement.addEventListener('mousemove', checktest);
        
        element.setAttribute("style","position: relative;border: 1px;display: inline-block; float: left; margin: 5px;overflow: hidden;");
        parentElement.setAttribute("appid", index);
       
      //  hAngle: getRandomExcept(0,360,1000,APPSEED + 222 + index
      //getRandomExcept(-90,90,100,APPSEED + 111+ index)
        APPSEED += 111;
        let vRandAngle = getRandomExcept(45,45,100,APPSEED + index);
        APPSEED += vRandAngle + 21;
        let vRandPhase = getRandomExcept(0,3,100,APPSEED + index);
        APPSEED += 222;
       /* let hRandAngle = getRandomExcept(45,45,100,APPSEED + index);
        APPSEED += hRandAngle + 97;*/
        applet.options.camPos = {vAngle: vRandAngle, hAngle: 45+(originalSeed % 4)*90};


        applet.options.body = randomBody;
      //  alert(applet.options.camPos.hAngle);



        if (document.location.pathname.includes("review") && !document.location.pathname.includes("preview")) {
            document.getElementsByClassName("visiblereviewonly")[0].style.display = "block";
            document.getElementsByClassName("visiblereviewonly")[1].style.display = "block";
            /*let containerElement = document.getElementById("solutionTablePlan");
            let t = "<table border=2 style=\"border: 2px solid #888; background-color: #fff;color: #333;font-family: cursive, verdana, monospace;font-weight: bold;text-align: center;margin: auto\">";
            for (let y=0; y<5; y++) {
                t += "<tr>";
                for (let x=0; x<5; x++) {

                    let num = plan[5*y + x];
                    t += "<td style=\"width: 32px; height: 32px;border: 2px solid #888; \">" + num + "</td>";
                }
                t += "</tr>";
            }
            t += "</table>";

            containerElement.innerHTML = t;*/

            applet.options.lookAround = true;
            document.querySelector(".outcome").style.display="none";
        }


        if (!(document.location.pathname.includes("review") && !document.location.pathname.includes("preview"))) {
            parentElement.addEventListener('mouseup', checktest);
            setInterval(checktest, 50);
        }

        if (index == 1) {
            
            if (nezet == 0) {
                applet.options.camPos = XPOSITIVEVIEW;
            } else {
                applet.options.camPos = ZPOSITIVEVIEW;
            }
        }


        applet.options.construction = true;
        let newApplet
        if (index != 1 || (document.location.pathname.includes("review") && !document.location.pathname.includes("preview")) ) {
            newApplet = new CubeApplet(element, applet.options);
        }

    
        

        if (index == 0) {

            if (nezet == 0) {
                newApplet.addObserver(XPOSITIVEVIEW, "white");
            } else {
                newApplet.addObserver(ZPOSITIVEVIEW, "white");
            }
            
        }
        
        appletArr.push(newApplet);
    }

    function rot90(p1) {
        let rotM = [4,9,14,19,24,3,8,13,18,23,2,7,12,17,22,1,6,11,16,21,0,5,10,15,20];
        let p2 = p1.slice();
        for (let i = 0; i < p1.length; i++) {
            p2[rotM[i]] = p1[i]
        }
        return p2;
    }

    function plansAreEqual(p1,p2) {
        for (let i = 0; i < p1.length; i++) {
            if (p1[i] != p2[i]) {
                return false;
            }
        }
        return true;
    }



    function getView(bodyArray) {
        let bba = JSON.parse(bodyArray);
        let res = [];

        if (nezet == 0) {
            for (let z = 4; z >= 0; z--) {
                for (let y = 0; y < 5; y++) {
                    let x = 4;
                    while (bba[x][y][z] == 0 && x>0){
                        x--;
                    }
                    res.push(bba[x][y][z]);
                }
            }
        } else {
            for (let y = 4; y >=0; y--) {
                for (let x = 0; x < 5; x++) {
                    let z = 4;
                    while (bba[x][y][z] == 0 && z>0){
                        z--;
                    }
                    res.push(bba[x][y][z]);
                }
            }
        }
        
        return res;
    }

    function checktest(e) {

       
        setTimeout(() => {
            let colorPaletteArr = ["#ffffff","#0000ff", "#008800","#ff9800", "#ff0000","5","#ffff00"];

            var planInputElements = document.getElementsByName('plan[]');

            var planInput = new Array(planInputElements.length);
            for (let i = 0; i < planInputElements.length; i++) {
                planInput[i] =  colorPaletteArr.indexOf(planInputElements[i].value);
            }


            let pr1 =  rot90(planInput)
            let pr2 =  rot90(pr1)
            let pr3 =  rot90(pr2)
       

            


            let questionState = {correct: JSON.stringify(planInput)==JSON.stringify(getView(randomBody)), result: planInput};
            let truefalseswitch = document.querySelector(".truefalseswitch > input");
            truefalseswitch.value = JSON.stringify(questionState);
       
          }, "100");
    }

});