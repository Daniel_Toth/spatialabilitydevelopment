<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * True-false question renderer class.
 *
 * @package    qtype
 * @subpackage vetulet_bontassal
 * @copyright  2009 The Open University
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */


defined('MOODLE_INTERNAL') || die();


/**
 * Generates the output for true-false questions.
 *
 * @copyright  2009 The Open University
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class qtype_vetulet_bontassal_renderer extends qtype_renderer {

    
    public function formulation_and_controls(question_attempt $qa,
            question_display_options $options) {
        global $PAGE, $CFG;

       
        //exit();

        echo "<script src=" . $CFG->wwwroot. '/question/type/vetulet_bontassal/scripts/three.min.js' . "></script>";
        echo "<script src=" . $CFG->wwwroot. '/question/type/vetulet_bontassal/scripts/builder.js' . "></script>";
        echo "<style>
        
        .formulation > input:checked + label.appid > div {
            box-shadow: 0 0 0px 4px #00ab00;
        }
        .formulation > input:checked + label.appid:after {
            content: \"\";
            box-shadow: none;
            color: #053400;
            position: absolute;
            margin-right: 20px;
            margin-top: 20px;
            width: 30px;
            height: 15px;
            border-bottom: solid 7px currentColor;
            border-left: solid 7px currentColor;
            -webkit-transform: rotate(-45deg);
            transform: rotate(-45deg);
            pointer-events: none;
            z-index: 10;
            top: 0;
            right: 0;
        }


        table.planeditor {
            border: 1px solid #888;
            background-color: #fff;
            color: #333;
            font-family: cursive, verdana, monospace;
            font-weight: bold;
            text-align: center;
            margin: auto;
        }


        table.planeditor td {
            width: 32px;
            height: 32px;
            border: 1px solid #888;
            position: relative;
            overflow: hidden;
        }

        table.planeditor input::selection {
            color: #fff;
            background: #fff;
          }

        table.planeditor input {
            outline: none;
            width: 80px;
            height: 80px;
            position: absolute;
            top: -25px;
            left: -20px;
            border: none;
            padding: 0;
            margin: 0;
            background-color: transparent;
        }



        .solutionContainer {
            display: flex;
            align-items: center;
            justify-content: space-evenly;
        }

        label.visiblereviewonly {
            width: 200px;
        }
        </style>";
      //  $PAGE->requires->js(new moodle_url($CFG->wwwroot. '/question/type/vetulet_bontassal/scripts/three.min.js'));
       // $PAGE->requires->js(new moodle_url($CFG->wwwroot. '/question/type/vetulet_bontassal/scripts/builder.js'));
        $PAGE->requires->js(new moodle_url($CFG->wwwroot. '/question/type/vetulet_bontassal/d.js'));
        $question = $qa->get_question();
        $response = $qa->get_last_qt_var('answer', '');
        $responseEncoded = json_decode($response);

        $inputname = $qa->get_qt_field_name('answer');
        $trueattributes = array(
            'type' => 'hidden',
            'name' => $inputname,
            'value' => $response,
            'id' => $inputname . 'true',
        );
      /*  $falseattributes = array(
            'type' => 'radio',
            'name' => $inputname,
            'value' => 0,
            'id' => $inputname . 'false',
        );*/

        if ($options->readonly) {
            $trueattributes['disabled'] = 'disabled';
            $falseattributes['disabled'] = 'disabled';
        }
/*
        // Work out which radio button to select (if any).
        $truechecked = false;
        $falsechecked = false;
        $responsearray = array();
        if ($response) {
            $trueattributes['checked'] = 'checked';
            $truechecked = true;
            $responsearray = array('answer' => 1);
        } else if ($response !== '') {
            $falseattributes['checked'] = 'checked';
            $falsechecked = true;
            $responsearray = array('answer' => 1);
        }
*/
        // Work out visual feedback for answer correctness.
        $trueclass = '';
        $falseclass = '';
        $truefeedbackimg = '';
        $falsefeedbackimg = '';
        /*if ($options->correctness) {
            if ($truechecked) {
                $trueclass = ' ' . $this->feedback_class((int) $question->rightanswer);
                $truefeedbackimg = $this->feedback_image((int) $question->rightanswer);
            } else if ($falsechecked) {
                $falseclass = ' ' . $this->feedback_class((int) (!$question->rightanswer));
                $falsefeedbackimg = $this->feedback_image((int) (!$question->rightanswer));
            }
        }*/

        /*$radiotrue = html_writer::empty_tag('input', $trueattributes) .
                html_writer::tag('label', get_string('true', 'qtype_vetulet_bontassal'),
                array('for' => $trueattributes['id'], 'class' => 'ml-1'));*/
        $elementid = "randomid" . bin2hex(random_bytes(10));
        $trueattributes["class"] .= $elementid;
        $radiotrue = html_writer::empty_tag('input', $trueattributes);

        $radiofalse = html_writer::empty_tag('input', $falseattributes) .
                html_writer::tag('label', get_string('false', 'qtype_vetulet_bontassal'),
                array('for' => $falseattributes['id'], 'class' => 'ml-1'));

        $questionData = json_decode($question->truefeedback);
        
        $axisColors = ["XPOSITIVEVIEW", "XNEGATIVEVIEW", "YPOSITIVEVIEW", "YNEGATIVEVIEW","ZPOSITIVEVIEW", "ZNEGATIVEVIEW"];
        /*
        if ($response != "") {
            $result = "<div id=\"".$elementid."alredysolved\" class=\"alredysolved\">A feladatot már egyszer megoldottad és az eredménye el lett mentve. $response Ha szeretnéd  újra megoldhatod, vagy lapozz egy másik feladatra.</div>";

        }*/
       
        

       // $result = "<div id=\"chooseBox\">";

      /* var_dump($responseEncoded);
       exit();*/
        for ($i = 0; $i < 1; $i++) {
                $result .= "<label for=\"checkbox$i\" id=\"".$axisColors[$i]. $elementid . "\" class=\"appid\"></label>";
                if ($i % 2 == 1) {
                    //$result .= "<br style=\"clear:both\">";
                }
        }
        $result .= "<div style=\"clear:both\"></div>
        <datalist id=\"presetColors\">
            <option>#ffffff</option>
            <option>#0000ff</option>
            <option>#008800</option>
            <option>#ff9800</option>
            <option>#ff0000</option>
            <option>#ffff00</option>
        </datalist>";

        $presetColors = array("#ffffff", "#0000ff", "#008800", "#ff9800","#ff0000","#ffffff","#ffff00");

        $result .= "<h3>Készítsd el az építmény vetületét a teknős irányából! Az ábrából elvehetsz kockákat, ha rájuk kattintasz. </h3>
        <ul>
        <li>A visszaállít gombra kattintva az építmény eredeti állapotba kerül. </li>
        <li>A helyes vetület elforgatottjai nincsenek elfogadva, a teknős helyzete meghatározza a helyes nézetet. </li>
        <li>A négyzetrács négyzeteire kattintva kiválaszthatod azok színét</li></ul><br>";
//        $result .= "<label id=\"".$axisColors[2]. $elementid . "\" class=\"appid\"></label><div style=\"clear:both\"></div>";
//   <input name=\"plan[]\" onchange=\"this.value= (parseInt(this.value)>= 0 && parseInt(this.value)<= 9)? parseInt(this.value):0 \" onClick=\"this.select()\" step=\"1\" min=\"0\" max=\"9\" value=\"$inputVal\" style=\"\" type=\"number\">
        $result .= "<div class=\"solutionContainer\"><label style=\"display: none\" class=\"visiblereviewonly\">Helyes megoldás (Az ábra forgatható)</label><div style=\"margin: 30px 0\"><label id=\"".$axisColors[1]. $elementid . "\" class=\"appid\"></label></div></div>";
        $result .=  "<div class=\"solutionContainer\"><label style=\"display: none\" class=\"visiblereviewonly\">A te megoldásod</label><div id=\"solutionTablePlan\" style=\"margin: 30px 0\"><table border=2 class=\"planeditor\">";
        for ($y=0; $y<5; $y++) {
            $result .=  "<tr>";
            for ($x=0; $x<5; $x++) {
                $inputVal = is_integer($responseEncoded->result[5*$y + $x]) ? $presetColors[$responseEncoded->result[5*$y + $x]] : "#ffffff";
                $result .=  "<td>
                <input name=\"plan[]\" type=\"color\"  onchange=\"if ((this.value!='#ffffff') && (this.value!='#0000ff') && (this.value!='#008800') && (this.value!='#ff9800') && (this.value!='#ff0000') && (this.value!='#ffff00')) { this.value='#ffffff';alert('Csak a felkínált színekből lehet választani.'); }\" value=\"$inputVal\" list=\"presetColors\">
                </td>";
            }
            $result .=  "</tr>";
        }
        $result .=  "</table></div></div>";



       // $result = "<div id=\"chooseBox\">";
      $result .= "<script>
        let APPSEED = $question->timecreated;
        const SOLUTION = \"" . $questionData->bodyJSON . "\"
        vetulet_bontassalImagePath  = typeof(vetulet_bontassalImagePath) == 'undefined' ? \"" . $CFG->wwwroot. "/question/type/vetulet_bontassal/pix/\" : vetulet_bontassalImagePath;
        appletList = typeof(appletList) == 'undefined' ? [] : appletList;";

        for ($i = 0; $i < 2; $i++) {
            $result .= "
            appletList.push({
                containerElementName: \"" . $axisColors[$i]. $elementid . "\",
                bodyJSON: SOLUTION,
                options: {
                    id: \"" . $axisColors[$i]. $elementid . "\",
                    width: 300,
                    height: 300,
                    cameraDistance: 20,
                    defaultZoom: 3.5,
                    projection:ORTHOGRAPHIC,
                    displayMode:  Display.Simple,
                    lookAround: false,
                    aimBlock: false,
                    zoomable: true,
                    construction: false,
                    limits: {xMin: -2,xMax: 2,yMin: -2,yMax: 2,zMin: 0,zMax: 5},
                    floor: {xStart: -2,xEnd: 2,yStart: -2, yEnd: 2},
                    camPos: STANDARDVIEW,
            }
            });";
        }

        $result .= "</script>";
        
       /* $result .= html_writer::tag('div',$question->truefeedback,
                array('class' => 'qtext'));*/

        $result .= html_writer::start_tag('div', array('class' => 'ablock'));
       /* $result .= html_writer::tag('div', get_string('selectone', 'qtype_vetulet_bontassal'),
                array('class' => 'prompt'));*/

        $result .= html_writer::start_tag('div', array('class' => 'answer'));

        $result .= html_writer::tag('div', $radiotrue . ' ' . $truefeedbackimg,array('class' => 'truefalseswitch'));


       /* $result .= html_writer::tag('div', $radiofalse . ' ' . $falsefeedbackimg,
                array('class' => 'r1' . $falseclass));*/
        $result .= html_writer::end_tag('div'); // Answer.

        $result .= html_writer::end_tag('div'); // Ablock.

        if ($qa->get_state() == question_state::$invalid) {
            $result .= html_writer::nonempty_tag('div',
                    $question->get_validation_error($responsearray),
                    array('class' => 'validationerror'));
        }

        return $result;
    }

    public function specific_feedback(question_attempt $qa) {
        $question = $qa->get_question();
        $response = $qa->get_last_qt_var('answer', '');

        if ($response) {
            return $question->format_text($question->truefeedback, $question->truefeedbackformat,
                    $qa, 'question', 'answerfeedback', $question->trueanswerid);
        } else if ($response !== '') {
            return $question->format_text($question->falsefeedback, $question->falsefeedbackformat,
                    $qa, 'question', 'answerfeedback', $question->falseanswerid);
        }
    }

    public function correct_response(question_attempt $qa) {
        $question = $qa->get_question();

        if ($question->rightanswer) {
            return get_string('correctanswertrue', 'qtype_vetulet_bontassal');
        } else {
            return get_string('correctanswerfalse', 'qtype_vetulet_bontassal');
        }
    }
}
