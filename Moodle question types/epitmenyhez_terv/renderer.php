<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * True-false question renderer class.
 *
 * @package    qtype
 * @subpackage epitmenyhez_terv
 * @copyright  2009 The Open University
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */


defined('MOODLE_INTERNAL') || die();


/**
 * Generates the output for true-false questions.
 *
 * @copyright  2009 The Open University
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class qtype_epitmenyhez_terv_renderer extends qtype_renderer {

    
    public function formulation_and_controls(question_attempt $qa,
            question_display_options $options) {
        global $PAGE, $CFG;

       
        //exit();

        echo "<script src=" . $CFG->wwwroot. '/question/type/epitmenyhez_terv/scripts/three.min.js' . "></script>";
        echo "<script src=" . $CFG->wwwroot. '/question/type/epitmenyhez_terv/scripts/builder.js' . "></script>";
        echo "<style>
        
        .formulation > input:checked + label.appid > div {
            box-shadow: 0 0 0px 4px #00ab00;
        }
        .formulation > input:checked + label.appid:after {
            content: \"\";
            box-shadow: none;
            color: #053400;
            position: absolute;
            margin-right: 20px;
            margin-top: 20px;
            width: 30px;
            height: 15px;
            border-bottom: solid 7px currentColor;
            border-left: solid 7px currentColor;
            -webkit-transform: rotate(-45deg);
            transform: rotate(-45deg);
            pointer-events: none;
            z-index: 10;
            top: 0;
            right: 0;
        }


        table.planeditor {
            border: 2px solid #888;
            background-color: #fff;
            color: #333;
            font-family: cursive, verdana, monospace;
            font-weight: bold;
            text-align: center;
            margin: auto;
        }


        table.planeditor td {
            width: 32px;
            height: 32px;
            border: 2px solid #888;
            position: relative;
        }

        table.planeditor input::selection {
            color: #fff;
            background: #fff;
          }

        table.planeditor input {
            border: none;
            outline: none;
            width: 40px;
            height: 32px;
            position: absolute;
            top: -2px;
            left: 0;
            padding-left: 9px;
            background-color: transparent
        }

        table.planeditor input:hover {
            z-index: 10;
        }


        .solutionContainer {
            display: flex;
            align-items: center;
            justify-content: space-evenly;
        }

        label.visiblereviewonly {
            width: 200px;
        }
        </style>";
      //  $PAGE->requires->js(new moodle_url($CFG->wwwroot. '/question/type/epitmenyhez_terv/scripts/three.min.js'));
       // $PAGE->requires->js(new moodle_url($CFG->wwwroot. '/question/type/epitmenyhez_terv/scripts/builder.js'));
        $PAGE->requires->js(new moodle_url($CFG->wwwroot. '/question/type/epitmenyhez_terv/d.js'));
        $question = $qa->get_question();
        $response = $qa->get_last_qt_var('answer', '');
        $responseEncoded = json_decode($response);

        $inputname = $qa->get_qt_field_name('answer');
        $trueattributes = array(
            'type' => 'hidden',
            'name' => $inputname,
            'value' => $response,
            'id' => $inputname . 'true',
        );
      /*  $falseattributes = array(
            'type' => 'radio',
            'name' => $inputname,
            'value' => 0,
            'id' => $inputname . 'false',
        );*/

        if ($options->readonly) {
            $trueattributes['disabled'] = 'disabled';
            $falseattributes['disabled'] = 'disabled';
        }
/*
        // Work out which radio button to select (if any).
        $truechecked = false;
        $falsechecked = false;
        $responsearray = array();
        if ($response) {
            $trueattributes['checked'] = 'checked';
            $truechecked = true;
            $responsearray = array('answer' => 1);
        } else if ($response !== '') {
            $falseattributes['checked'] = 'checked';
            $falsechecked = true;
            $responsearray = array('answer' => 1);
        }
*/
        // Work out visual feedback for answer correctness.
        $trueclass = '';
        $falseclass = '';
        $truefeedbackimg = '';
        $falsefeedbackimg = '';
        /*if ($options->correctness) {
            if ($truechecked) {
                $trueclass = ' ' . $this->feedback_class((int) $question->rightanswer);
                $truefeedbackimg = $this->feedback_image((int) $question->rightanswer);
            } else if ($falsechecked) {
                $falseclass = ' ' . $this->feedback_class((int) (!$question->rightanswer));
                $falsefeedbackimg = $this->feedback_image((int) (!$question->rightanswer));
            }
        }*/

        /*$radiotrue = html_writer::empty_tag('input', $trueattributes) .
                html_writer::tag('label', get_string('true', 'qtype_epitmenyhez_terv'),
                array('for' => $trueattributes['id'], 'class' => 'ml-1'));*/
        $elementid = "randomid" . bin2hex(random_bytes(10));
        $trueattributes["class"] .= $elementid;
        $radiotrue = html_writer::empty_tag('input', $trueattributes);

        $radiofalse = html_writer::empty_tag('input', $falseattributes) .
                html_writer::tag('label', get_string('false', 'qtype_epitmenyhez_terv'),
                array('for' => $falseattributes['id'], 'class' => 'ml-1'));

        $questionData = json_decode($question->truefeedback);
        
        $axisColors = ["XPOSITIVEVIEW", "XNEGATIVEVIEW", "YPOSITIVEVIEW", "YNEGATIVEVIEW","ZPOSITIVEVIEW", "ZNEGATIVEVIEW"];

        $result = "<h4 id=\"showInReview\" style=\"display:none\">Az építmények az egérrel forgathatók</h4>";
        for ($i = 0; $i < 2; $i++) {
                $result .= "<label for=\"checkbox$i\" id=\"".$axisColors[$i]. $elementid . "\" class=\"appid\"></label>";
                if ($i % 2 == 1) {
                    //$result .= "<br style=\"clear:both\">";
                }
        }
        $result .= "<div style=\"clear:both\"></div>";
        $result .= "<h3>A két ábrán ugyanaz az építmény látható. Készíts tervet az építményről!</h3><span>(A számokat a négyzetrácsba  kattintva írhatod be. A terv bármelyik forgatottja helyes megoldás.)</span><br>";

        $result .= "<div class=\"solutionContainer\"><label style=\"display: none\" class=\"visiblereviewonly\">Helyes megoldások</label><div id=\"solutionTablePlan\" style=\"margin: 30px 0\"></div></div>";
        $result .=  "<div class=\"solutionContainer\"><label style=\"display: none\" class=\"visiblereviewonly\">A te megoldásod</label><div id=\"solutionTablePlan\" style=\"margin: 30px 0\"><table border=2 class=\"planeditor\">";
        for ($y=0; $y<5; $y++) {
            $result .=  "<tr>";
            for ($x=0; $x<5; $x++) {
                $inputVal = is_integer($responseEncoded->result[5*$y + $x]) ? $responseEncoded->result[5*$y + $x] : 0;

                $colorCell = ["#fff","#c8ffc8","#afa","#7f7","#5f5","#3f3","#fff","#fff","#fff","#fff","#fff","#fff","#fff","#fff","#fff","#fff"];
                $cssbackground = "background-color:" . $colorCell[$inputVal];

                


                $result .=  "<td  style=\"$cssbackground\">
                <input name=\"plan[]\" onchange=\"this.value= (parseInt(this.value)>= 0 && parseInt(this.value)<= 9)? parseInt(this.value):0 \" onClick=\"this.select()\" step=\"1\" min=\"0\" max=\"9\" value=\"$inputVal\" type=\"number\">
                </td>";
            }
            $result .=  "</tr>";
        }
        $result .=  "</table></div></div>";



       // $result = "<div id=\"chooseBox\">";
      $result .= "<script>
        let APPSEED = $question->timecreated;
        const SOLUTION = \"" . $questionData->bodyJSON . "\"
        epitmenyhez_tervImagePath  = typeof(epitmenyhez_tervImagePath) == 'undefined' ? \"" . $CFG->wwwroot. "/question/type/epitmenyhez_terv/pix/\" : epitmenyhez_tervImagePath;
        appletList = typeof(appletList) == 'undefined' ? [] : appletList;";

        for ($i = 0; $i < 2; $i++) {
            $result .= "
            appletList.push({
                containerElementName: \"" . $axisColors[$i]. $elementid . "\",
                bodyJSON: SOLUTION,
                options: {
                    id: \"" . $axisColors[$i]. $elementid . "\",
                    width: 300,
                    height: 300,
                    cameraDistance: 20,
                    defaultZoom: 3.5,
                    projection:ORTHOGRAPHIC,
                    displayMode:  Display.TextBook,
                    lookAround: false,
                    aimBlock: false,
                    zoomable: true,
                    construction: false,
                    limits: {xMin: -2,xMax: 2,yMin: -2,yMax: 2,zMin: 0,zMax: 5},
                    floor: {xStart: -2,xEnd: 2,yStart: -2, yEnd: 2},
                    camPos: STANDARDVIEW,
            }
            });";
        }

        $result .= "</script>";
        
       /* $result .= html_writer::tag('div',$question->truefeedback,
                array('class' => 'qtext'));*/

        $result .= html_writer::start_tag('div', array('class' => 'ablock'));
       /* $result .= html_writer::tag('div', get_string('selectone', 'qtype_epitmenyhez_terv'),
                array('class' => 'prompt'));*/

        $result .= html_writer::start_tag('div', array('class' => 'answer'));

        $result .= html_writer::tag('div', $radiotrue . ' ' . $truefeedbackimg,array('class' => 'truefalseswitch'));


       /* $result .= html_writer::tag('div', $radiofalse . ' ' . $falsefeedbackimg,
                array('class' => 'r1' . $falseclass));*/
        $result .= html_writer::end_tag('div'); // Answer.

        $result .= html_writer::end_tag('div'); // Ablock.

        if ($qa->get_state() == question_state::$invalid) {
            $result .= html_writer::nonempty_tag('div',
                    $question->get_validation_error($responsearray),
                    array('class' => 'validationerror'));
        }

        return $result;
    }

    public function specific_feedback(question_attempt $qa) {
        $question = $qa->get_question();
        $response = $qa->get_last_qt_var('answer', '');

        if ($response) {
            return $question->format_text($question->truefeedback, $question->truefeedbackformat,
                    $qa, 'question', 'answerfeedback', $question->trueanswerid);
        } else if ($response !== '') {
            return $question->format_text($question->falsefeedback, $question->falsefeedbackformat,
                    $qa, 'question', 'answerfeedback', $question->falseanswerid);
        }
    }

    public function correct_response(question_attempt $qa) {
        $question = $qa->get_question();

        if ($question->rightanswer) {
            return get_string('correctanswertrue', 'qtype_epitmenyhez_terv');
        } else {
            return get_string('correctanswerfalse', 'qtype_epitmenyhez_terv');
        }
    }
}
